package name.evdubs.hermaea;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.junit.Test;

public class BitfinexHedgerTest {

  @Test
  public void test() {
    BlockingQueue<BitfinexCurrencyPairOrderBook> btcUsdHedgerQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
    BlockingQueue<BitfinexCurrencyPairOrderBook> ltcUsdHedgerQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
    BlockingQueue<BitfinexCurrencyPairOrderBook> ltcBtcHedgerQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
    
    BlockingQueue<CurrencyPairBookDepthData> btcUsdDepthDataQueue = new LinkedBlockingDeque<CurrencyPairBookDepthData>();
    BlockingQueue<CurrencyPairBookDepthData> ltcUsdDepthDataQueue = new LinkedBlockingDeque<CurrencyPairBookDepthData>();
    BlockingQueue<CurrencyPairBookDepthData> ltcBtcDepthDataQueue = new LinkedBlockingDeque<CurrencyPairBookDepthData>();
    
    List<BlockingQueue<BitfinexCurrencyPairOrderBook>> btcUsdOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
    //btcUsdOrderBookQueues.add(btcUsdHedgerQueue);
    List<BlockingQueue<BitfinexCurrencyPairOrderBook>> ltcUsdOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
    //ltcUsdOrderBookQueues.add(ltcUsdHedgerQueue);
    List<BlockingQueue<BitfinexCurrencyPairOrderBook>> ltcBtcOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
    //ltcBtcOrderBookQueues.add(ltcBtcHedgerQueue);
    
    BlockingQueue<OrderData> btcUsdOrderDataQueue = new LinkedBlockingDeque<OrderData>();
    BlockingQueue<OrderData> ltcUsdOrderDataQueue = new LinkedBlockingDeque<OrderData>();
    BlockingQueue<OrderData> ltcBtcOrderDataQueue = new LinkedBlockingDeque<OrderData>();
    
    BlockingQueue<PositionData> positionDataQueue = new LinkedBlockingDeque<PositionData>();
    
    try {
      BitfinexCurrencyPairOrderBook btcUsd = new BitfinexCurrencyPairOrderBook("BTCUSD", btcUsdDepthDataQueue, btcUsdOrderDataQueue,
          btcUsdOrderBookQueues);
      BitfinexCurrencyPairOrderBook ltcUsd = new BitfinexCurrencyPairOrderBook("LTCUSD", ltcUsdDepthDataQueue, ltcUsdOrderDataQueue,
          ltcUsdOrderBookQueues);
      BitfinexCurrencyPairOrderBook ltcBtc = new BitfinexCurrencyPairOrderBook("LTCBTC", ltcBtcDepthDataQueue, ltcBtcOrderDataQueue,
          ltcBtcOrderBookQueues);
      
      Map<String, BitfinexCurrencyPairOrderBook> orderBooks = new HashMap<String, BitfinexCurrencyPairOrderBook>();
      orderBooks.put("BTCUSD", btcUsd);
      orderBooks.put("LTCUSD", ltcUsd);
      orderBooks.put("LTCBTC", ltcBtc);
      
      /*List<BlockingQueue<BitfinexCurrencyPairOrderBook>> hedgerOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
      hedgerOrderBookQueues.add(btcUsdHedgerQueue);
      hedgerOrderBookQueues.add(ltcUsdHedgerQueue);
      hedgerOrderBookQueues.add(ltcBtcHedgerQueue);*/
      
      BitfinexHedger hedger = new BitfinexHedger(orderBooks, positionDataQueue);
      
      // Market 1
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("110.0"), BidAsk.ASK, new BigDecimal("2"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("90.0"), BidAsk.BID, new BigDecimal("2"), new Date()));
      ltcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("LTCUSD", new BigDecimal("5.5"), BidAsk.ASK, new BigDecimal("40"), new Date()));
      ltcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("LTCUSD", new BigDecimal("4.5"), BidAsk.BID, new BigDecimal("40"), new Date()));
      ltcBtcDepthDataQueue.put(new CurrencyPairBookDepthData("LTCBTC", new BigDecimal("0.055"), BidAsk.ASK, new BigDecimal("40"), new Date()));
      ltcBtcDepthDataQueue.put(new CurrencyPairBookDepthData("LTCBTC", new BigDecimal("0.045"), BidAsk.BID, new BigDecimal("40"), new Date()));
      
      // Scenario 1
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("1"), LongShort.LONG, new BigDecimal("90"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100); // sleep to make sure the work is done
      assertTrue(hedger.getHedgeOrders().isEmpty());
      
      // Scenario 2
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("0"), LongShort.LONG, new BigDecimal("0"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("20"), LongShort.LONG, new BigDecimal("4.5"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().isEmpty());
      
      // Scenario 3
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("1"), LongShort.LONG, new BigDecimal("90"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().isEmpty());
      
      // Scenario 4
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("20"), LongShort.SHORT, new BigDecimal("5.5"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().isEmpty());
      
      // Market 2
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("110.0"), BidAsk.ASK, new BigDecimal("0"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("90.0"), BidAsk.BID, new BigDecimal("0"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("100.0"), BidAsk.ASK, new BigDecimal("2"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("100.0"), BidAsk.BID, new BigDecimal("2"), new Date()));
      
      // Scenario 1
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("0"), LongShort.SHORT, new BigDecimal("0"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("1"), LongShort.LONG, new BigDecimal("90"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100); // sleep to make sure the work is done
      assertTrue(hedger.getHedgeOrders().size() == 1);
      
      // Scenario 2
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("0"), LongShort.LONG, new BigDecimal("0"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("20"), LongShort.LONG, new BigDecimal("4.5"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().isEmpty());
      
      // Scenario 3
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("1"), LongShort.LONG, new BigDecimal("90"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().size() == 2);
      
      // Scenario 4
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("20"), LongShort.SHORT, new BigDecimal("5.5"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().size() == 2);
      
      // Market 3
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("100.0"), BidAsk.ASK, new BigDecimal("0"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("100.0"), BidAsk.BID, new BigDecimal("0"), new Date()));
      ltcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("LTCUSD", new BigDecimal("5.5"), BidAsk.ASK, new BigDecimal("0"), new Date()));
      ltcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("LTCUSD", new BigDecimal("4.5"), BidAsk.BID, new BigDecimal("0"), new Date()));
      ltcBtcDepthDataQueue.put(new CurrencyPairBookDepthData("LTCBTC", new BigDecimal("0.055"), BidAsk.ASK, new BigDecimal("0"), new Date()));
      ltcBtcDepthDataQueue.put(new CurrencyPairBookDepthData("LTCBTC", new BigDecimal("0.045"), BidAsk.BID, new BigDecimal("0"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("110.0"), BidAsk.ASK, new BigDecimal("2"), new Date()));
      btcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("BTCUSD", new BigDecimal("90.0"), BidAsk.BID, new BigDecimal("2"), new Date()));
      ltcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("LTCUSD", new BigDecimal("5.0"), BidAsk.ASK, new BigDecimal("40"), new Date()));
      ltcUsdDepthDataQueue.put(new CurrencyPairBookDepthData("LTCUSD", new BigDecimal("5.0"), BidAsk.BID, new BigDecimal("40"), new Date()));
      ltcBtcDepthDataQueue.put(new CurrencyPairBookDepthData("LTCBTC", new BigDecimal("0.050"), BidAsk.ASK, new BigDecimal("40"), new Date()));
      ltcBtcDepthDataQueue.put(new CurrencyPairBookDepthData("LTCBTC", new BigDecimal("0.050"), BidAsk.BID, new BigDecimal("40"), new Date()));

      // Scenario 1
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("0"), LongShort.SHORT, new BigDecimal("0"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("1"), LongShort.LONG, new BigDecimal("90"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100); // sleep to make sure the work is done
      assertTrue(hedger.getHedgeOrders().size() == 0);
      
      // Scenario 2
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("0"), LongShort.LONG, new BigDecimal("0"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("20"), LongShort.LONG, new BigDecimal("4.5"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().size() == 1);
      
      // Scenario 3
      positionDataQueue.put(new PositionData("BTCUSD", "active", new BigDecimal("1"), LongShort.LONG, new BigDecimal("90"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().size() == 2);
      
      // Scenario 4
      positionDataQueue.put(new PositionData("LTCUSD", "active", new BigDecimal("20"), LongShort.SHORT, new BigDecimal("5.5"), new BigDecimal("0"), MarginFundingSchedule.DAILY));
      Thread.sleep(100);
      assertTrue(hedger.getHedgeOrders().size() == 2);
      
    } catch (InvalidCurrencyCodeException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
