package name.evdubs.hermaea;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SwapBookDepthData extends BitfinexWebSocketData {
  protected final String currency;
  protected final BigDecimal rate;
  protected final int period;
  protected final String bidAsk;
  protected final BigDecimal totalAmount;
  protected final BigDecimal amountChanged;
  protected final double timestamp;
  
  public SwapBookDepthData(@JsonProperty("currency") String currency, @JsonProperty("price") BigDecimal rate, @JsonProperty("period") int period, @JsonProperty("type") String type, @JsonProperty("total_amount") BigDecimal totalAmount, @JsonProperty("change") BigDecimal change, @JsonProperty("timestamp") double timestamp) {
    this.currency = currency;
    this.rate = rate;
    this.period = period;
    this.bidAsk = type;
    this.totalAmount = totalAmount;
    this.amountChanged = change;
    this.timestamp = timestamp;
  }

  public String getCurrency() {
    return currency;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public int getPeriod() {
    return period;
  }

  public String getBidAsk() {
    return bidAsk;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public BigDecimal getAmountChanged() {
    return amountChanged;
  }

  public double getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return "SwapBookDepthData [currency=" + currency + ", rate=" + rate + ", period=" + period + ", bidAsk=" + bidAsk + ", totalAmount=" + totalAmount + ", amountChanged=" + amountChanged
        + ", timestamp=" + timestamp + "]";
  }
  
}
