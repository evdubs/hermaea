package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

public class ImpliedBitfinexCurrencyPairOrderBook {
  // quote currency "price" -> base currency "amount"
  protected volatile ConcurrentNavigableMap<Money, Money> priceBidMap = new ConcurrentSkipListMap<Money, Money>();
  protected volatile ConcurrentNavigableMap<Money, Money> priceAskMap = new ConcurrentSkipListMap<Money, Money>();

  protected final CurrencyUnit baseCurrency;
  protected final CurrencyUnit counterCurrency;
  
  protected final BlockingQueue<BitfinexCurrencyPairOrderBook> orderBookQueue;
  protected final BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> impliedBookQueue;

  public ImpliedBitfinexCurrencyPairOrderBook(CurrencyUnit baseCurrency,
      CurrencyUnit counterCurrency, BlockingQueue<BitfinexCurrencyPairOrderBook> orderBookQueue, BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> impliedBookQueue) {
    this.baseCurrency = baseCurrency;
    this.counterCurrency = counterCurrency;
    this.orderBookQueue = orderBookQueue;
    this.impliedBookQueue = impliedBookQueue;

    Thread t = new Thread(new OrderBookQueueConsumer(), OrderBookQueueConsumer.class.getSimpleName());
    t.start();
  }

  public CurrencyUnit getBaseCurrency() {
    return baseCurrency;
  }

  public CurrencyUnit getCounterCurrency() {
    return counterCurrency;
  }

  public ConcurrentNavigableMap<Money, Money> getBids() {
    return priceBidMap;
  }

  public ConcurrentNavigableMap<Money, Money> getAsks() {
    return priceAskMap;
  }
  
  protected ConcurrentNavigableMap<Money, Money> invertMap(ConcurrentNavigableMap<Money, Money> map) {
    ConcurrentNavigableMap<Money, Money> newMap;
    // ugly! null comparator means that it uses the natural ordering, which we need to reverse
    if (map.comparator() == null) {
      newMap = new ConcurrentSkipListMap<Money, Money>(new Comparator<Money>() {
        public int compare(Money o1, Money o2) {
          return o2.compareTo(o1);
        }
      });
    } else { // should be reverse ordering so we can just use natural ordering; bug prone!
      newMap = new ConcurrentSkipListMap<Money, Money>();
    }
    
    for(Entry<Money, Money> entry : map.entrySet()) {
      Money one = Money.of(entry.getValue().getCurrencyUnit(), new BigDecimal("1"));
      newMap.put(one.dividedBy(entry.getKey().getAmount(), RoundingMode.HALF_EVEN), 
          entry.getKey().multipliedBy(entry.getValue().getAmount(), RoundingMode.HALF_EVEN));
    }
    
    return newMap;
  }
  
  protected ConcurrentNavigableMap<Money, Money> implyMap(ConcurrentNavigableMap<Money, Money> ab, 
      ConcurrentNavigableMap<Money, Money> bc) {
    ConcurrentNavigableMap<Money, Money> ac;
    
    if (ab.comparator() != null && bc.comparator() != null ) {
      ac = new ConcurrentSkipListMap<Money, Money>(new Comparator<Money>() {
        public int compare(Money arg0, Money arg1) {
          return arg1.compareTo(arg0);
        }
      });
    } else if (ab.comparator() == null && bc.comparator() == null) {
      ac = new ConcurrentSkipListMap<Money, Money>();
    } else {
      System.out.println("Have differing comparators");
      return null;
    }
    
    Iterator<Entry<Money, Money>> abIter = ab.entrySet().iterator();
    // initial currency doesn't matter as the loop will set the appropriate amounts and currencies
    Money abPrice = Money.of(baseCurrency, BigDecimal.ZERO);
    Money abAmount = Money.of(baseCurrency, BigDecimal.ZERO);
    
    Iterator<Entry<Money, Money>> bcIter = bc.entrySet().iterator();
    Money bcPrice = Money.of(baseCurrency, BigDecimal.ZERO);
    Money bcAmount = Money.of(baseCurrency, BigDecimal.ZERO);
    
    while (abIter.hasNext() && bcIter.hasNext()) {
      if (BigDecimal.ZERO.compareTo(abAmount.getAmount()) == 0) {
        Entry<Money, Money> abEntry = abIter.next();
        abAmount = abEntry.getValue();
        abPrice = abEntry.getKey();
      }
      
      if (BigDecimal.ZERO.compareTo(bcAmount.getAmount()) == 0) {
        Entry<Money, Money> bcEntry = bcIter.next();
        bcAmount = bcEntry.getValue();
        bcPrice = bcEntry.getKey();
      }
      
      // as with the above, currency doesn't matter when setting amount to zero
      if (BigDecimal.ZERO.compareTo(abPrice.getAmount()) == 0) {
        abAmount = Money.of(baseCurrency, BigDecimal.ZERO);
      } else if (BigDecimal.ZERO.compareTo(bcPrice.getAmount()) == 0) {
        bcAmount = Money.of(baseCurrency, BigDecimal.ZERO);
      } else {
        // we're sure we have nonzero prices to compute for our map
        Money abAmountInB = abPrice.multipliedBy(abAmount.getAmount(), RoundingMode.HALF_EVEN);
        
        if (abAmountInB.compareTo(bcAmount) >= 0) {
          Money bcAmountInA = Money.of(abAmount.getCurrencyUnit(), bcAmount.dividedBy(abPrice.getAmount(), RoundingMode.HALF_EVEN).getAmount());
          
          ac.put(bcPrice.multipliedBy(abPrice.getAmount(), RoundingMode.HALF_EVEN), bcAmountInA);
          abAmount = abAmount.minus(bcAmountInA);
          bcAmount = bcAmount.minus(bcAmount); // aka zero
        } else { // abAmountInB < bcAmount
          ac.put(bcPrice.multipliedBy(abPrice.getAmount(), RoundingMode.HALF_EVEN), abAmount);
          abAmount = abAmount.minus(abAmount); // aka zero
          bcAmount = bcAmount.minus(abAmountInB);
        }
      }
    }
    
    return ac;
  }

  protected void calculateImpliedBook(BitfinexCurrencyPairOrderBook firstBook, BitfinexCurrencyPairOrderBook secondBook) 
      throws InvalidBooksForImpliedBookException {
    ConcurrentNavigableMap<Money, Money> baseTertBidMap;
    ConcurrentNavigableMap<Money, Money> tertCounterBidMap;
    ConcurrentNavigableMap<Money, Money> baseTertAskMap;
    ConcurrentNavigableMap<Money, Money> tertCounterAskMap;
    
    if (firstBook.getBaseCurrency().equals(baseCurrency)) {
      if (secondBook.getBaseCurrency().equals(counterCurrency)) {
        if (firstBook.getCounterCurrency().equals(secondBook.getCounterCurrency())) {
          // implied BTCUSD
          // first book BTCETH
          // second book USDETH
          // implied bids are first book bids and inverted second book asks
          baseTertBidMap = firstBook.getForeignBids();
          tertCounterBidMap = invertMap(secondBook.getForeignAsks());
          // implied asks are first book asks and inverted second book bids
          baseTertAskMap = firstBook.getForeignAsks();
          tertCounterAskMap = invertMap(secondBook.getForeignBids());
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else if (secondBook.getCounterCurrency().equals(counterCurrency)) {
        if (firstBook.getCounterCurrency().equals(secondBook.getBaseCurrency())) {
          // implied BTCUSD
          // first book BTCETH
          // second book ETHUSD
          baseTertBidMap = firstBook.getForeignBids();
          tertCounterBidMap = secondBook.getForeignBids();
          baseTertAskMap = firstBook.getForeignAsks();
          tertCounterAskMap = secondBook.getForeignAsks();
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else {
        throw new InvalidBooksForImpliedBookException("Could not find counter currency in second book");
      }
    } else if (firstBook.getCounterCurrency().equals(baseCurrency)) {
      if (secondBook.getBaseCurrency().equals(counterCurrency)) {
        if (firstBook.getBaseCurrency().equals(secondBook.getCounterCurrency())) {
          // implied BTCUSD
          // first book ETHBTC
          // second book USDETH
          // implied bids are inverted first book asks and inverted second book asks
          baseTertBidMap = invertMap(firstBook.getForeignAsks());
          tertCounterBidMap = invertMap(secondBook.getForeignAsks());
          baseTertAskMap = invertMap(firstBook.getForeignBids());
          tertCounterAskMap = invertMap(secondBook.getForeignBids());
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else if (secondBook.getCounterCurrency().equals(counterCurrency)) {
        if (firstBook.getBaseCurrency().equals(secondBook.getBaseCurrency())) {
          // implied BTCUSD
          // first book ETHBTC
          // second book ETHUSD
          // implied bids are inverted first book asks and second book bids
          baseTertBidMap = invertMap(firstBook.getForeignAsks());
          tertCounterBidMap = secondBook.getForeignBids();
          baseTertAskMap = invertMap(firstBook.getForeignBids());
          tertCounterAskMap = secondBook.getForeignAsks();
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else {
        throw new InvalidBooksForImpliedBookException("Could not find counter currency in second book");
      }
    } else if (secondBook.getBaseCurrency().equals(baseCurrency)) {
      if (firstBook.getBaseCurrency().equals(counterCurrency)) {
        if (secondBook.getCounterCurrency().equals(firstBook.getCounterCurrency())) {
          // implied BTCUSD
          // second book BTCETH
          // first book USDETH
          // implied bids are second book bids and inverted first book asks
          baseTertBidMap = secondBook.getForeignBids();
          tertCounterBidMap = invertMap(firstBook.getForeignAsks());
          baseTertAskMap = secondBook.getForeignAsks();
          tertCounterAskMap = invertMap(firstBook.getForeignBids());
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else if (firstBook.getCounterCurrency().equals(counterCurrency)) {
        if (secondBook.getCounterCurrency().equals(firstBook.getBaseCurrency())) {
          // implied BTCUSD
          // second book BTCETH
          // first book ETHUSD
          // implied bids are second book bids and first book bids
          baseTertBidMap = secondBook.getForeignBids();
          tertCounterBidMap = firstBook.getForeignBids();
          baseTertAskMap = secondBook.getForeignAsks();
          tertCounterAskMap = firstBook.getForeignAsks();
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else {
        throw new InvalidBooksForImpliedBookException("Could not find counter currency in first book");
      }
    } else if (secondBook.getCounterCurrency().equals(baseCurrency)) {
      if (firstBook.getBaseCurrency().equals(counterCurrency)) {
        if (secondBook.getBaseCurrency().equals(firstBook.getCounterCurrency())) {
          // implied BTCUSD
          // second book ETHBTC
          // first book USDETH
          // implied bids are inverted second book asks and inverted first book asks
          baseTertBidMap = invertMap(secondBook.getForeignAsks());
          tertCounterBidMap = invertMap(firstBook.getForeignAsks());
          baseTertAskMap = invertMap(secondBook.getForeignBids());
          tertCounterAskMap = invertMap(firstBook.getForeignBids());
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else if (firstBook.getCounterCurrency().equals(counterCurrency)) {
        if (secondBook.getBaseCurrency().equals(firstBook.getBaseCurrency())) {
          // implied BTCUSD
          // second book is ETHBTC
          // first book is ETHUSD
          // implied bids are inverted second book asks and first book bids
          baseTertBidMap = invertMap(secondBook.getForeignAsks());
          tertCounterBidMap = firstBook.getForeignBids();
          baseTertAskMap = invertMap(secondBook.getForeignBids());
          tertCounterAskMap = firstBook.getForeignAsks();
        } else {
          throw new InvalidBooksForImpliedBookException("Could not find a common currency between books");
        }
      } else {
        throw new InvalidBooksForImpliedBookException("Could not find counter currency in first book");
      }
    } else {
      throw new InvalidBooksForImpliedBookException("Could not find base currency");
    }

    priceBidMap = implyMap(baseTertBidMap, tertCounterBidMap);
    priceAskMap = implyMap(baseTertAskMap, tertCounterAskMap);
    
    try {
      impliedBookQueue.put(this);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  protected class OrderBookQueueConsumer implements Runnable {
    BitfinexCurrencyPairOrderBook firstBook;
    BitfinexCurrencyPairOrderBook secondBook;
    
    public void run() {
      while (true) {
        try {
          BitfinexCurrencyPairOrderBook book = orderBookQueue.take();
          if (firstBook == null)
            firstBook = book;
          else if (secondBook == null && firstBook != book)
            secondBook = book;
          
          if (firstBook != null && secondBook != null)
            calculateImpliedBook(firstBook, secondBook);
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (InvalidBooksForImpliedBookException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
