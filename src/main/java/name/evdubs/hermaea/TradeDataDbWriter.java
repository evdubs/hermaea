package name.evdubs.hermaea;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TradeDataDbWriter implements DataHandler<CurrencyPairTradeData> {
  private BlockingQueue<CurrencyPairTradeData> tradeQueue;

  public TradeDataDbWriter() {
    tradeQueue = new LinkedBlockingQueue<CurrencyPairTradeData>();
    TradeDataDbWriterRunnable tradeRunnable = new TradeDataDbWriterRunnable(
        tradeQueue);

    (new Thread(tradeRunnable)).start();
  }

  public void onData(CurrencyPairTradeData data) {
    try {
      tradeQueue.put(data);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private class TradeDataDbWriterRunnable implements Runnable {
    private BlockingQueue<CurrencyPairTradeData> tradeQueue;

    public TradeDataDbWriterRunnable(
        BlockingQueue<CurrencyPairTradeData> tradeQueue) {
      this.tradeQueue = tradeQueue;
    }

    public void run() {
      PreparedStatement ps;
      try {
        Connection c = DriverManager.getConnection("jdbc:postgresql://10.0.0.3:5432/", "postgres", "");
        ps = c.prepareStatement("insert into pair_trade_data "
            + "values(?::text, ?::numeric, ?::numeric, ?::text, ?::float)");
      } catch (SQLException e1) {
        e1.printStackTrace();
        return;
      }
      
      while (true) {
        try {
          CurrencyPairTradeData data = tradeQueue.take();

          ps.setString(1, data.getCurrencyPair());
          ps.setBigDecimal(2, data.getPrice());
          ps.setBigDecimal(3, data.getAmount());
          ps.setString(4, data.getSide() == 0 ? "buy" : "sell");
          ps.setDouble(5, data.getTimestamp());
          
          ps.execute();
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
