package name.evdubs.hermaea;

import java.math.BigDecimal;

public class WalletData {
  
  protected final String name;
  protected final String currency;
  protected final BigDecimal balance;
  protected final BigDecimal unsettledInterest;
  
  public WalletData(String name, String currency, BigDecimal balance, BigDecimal unsettledInterest) {
    
    this.name = name;
    this.currency = currency;
    this.balance = balance;
    this.unsettledInterest = unsettledInterest;
  }
  
  public static WalletData create(String[] array) {
    
    String name = array[0];
    String currency = array[1];
    BigDecimal balance = new BigDecimal(array[2]);
    BigDecimal unsettledInterest = new BigDecimal(array[3]);
    
    return new WalletData(name, currency, balance, unsettledInterest);
  }

  public String getName() {
    
    return name;
  }

  public String getCurrency() {
    
    return currency;
  }

  public BigDecimal getBalance() {
    
    return balance;
  }

  public BigDecimal getUnsettledInterest() {
    
    return unsettledInterest;
  }

  @Override
  public String toString() {
    
    return "WalletData [name=" + name + ", currency=" + currency + ", balance=" + balance + ", unsettledInterest=" + unsettledInterest + "]";
  }
  
}
