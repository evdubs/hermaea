package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.joda.money.Money;

import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.trade.LimitOrder;

public class ImpliedCurrencyPairQuoter {
  protected final BigDecimal bidEdgeMultiplier = new BigDecimal("0.9925");
  protected final BigDecimal askEdgeMultiplier = new BigDecimal("1.0075");
  
  protected final Money bucketSize;
  protected final Money sideMaxAmount;
  protected final BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> impliedBookQueue;
  protected volatile ConcurrentNavigableMap<Money, LimitOrder> quoterBidOrders;
  protected volatile ConcurrentNavigableMap<Money, LimitOrder> quoterAskOrders;
  
  public ImpliedCurrencyPairQuoter(Money bucketSize, Money sideMaxAmount, BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> impliedBookQueue) {
    this.bucketSize = bucketSize;
    this.sideMaxAmount = sideMaxAmount;
    this.impliedBookQueue = impliedBookQueue;
    
    quoterBidOrders = new ConcurrentSkipListMap<Money, LimitOrder>();
    quoterAskOrders = new ConcurrentSkipListMap<Money, LimitOrder>();
    
    Thread t = new Thread(new ImpliedBookConsumer(), ImpliedBookConsumer.class.getSimpleName());
    t.start();
  }

  public ConcurrentNavigableMap<Money, LimitOrder> getQuoterBidOrders() {
    return quoterBidOrders;
  }

  public ConcurrentNavigableMap<Money, LimitOrder> getQuoterAskOrders() {
    return quoterAskOrders;
  }

  protected void calculateQuotes(ImpliedBitfinexCurrencyPairOrderBook book) {
    ConcurrentNavigableMap<Money, LimitOrder> bidOrders = new ConcurrentSkipListMap<Money, LimitOrder>();
    
    // decimal places to scale to for prices
    int scale = 8;
    if (Hermaea.BTC.equals(book.getBaseCurrency()) && Hermaea.USD.equals(book.getCounterCurrency()))
      scale = 2;
    else if (Hermaea.ETH.equals(book.getBaseCurrency()) && Hermaea.USD.equals(book.getCounterCurrency()))
      scale = 3;
    else if (Hermaea.ETH.equals(book.getBaseCurrency()) && Hermaea.BTC.equals(book.getCounterCurrency()))
      scale = 6;

    Money bucketAveragePrice = Money.of(book.getCounterCurrency(), BigDecimal.ZERO);
    Money bucketAccumulatedAmount = Money.of(book.getBaseCurrency(), BigDecimal.ZERO);
    Money totalAccumulatedAmount = Money.of(book.getBaseCurrency(), BigDecimal.ZERO);

    for (Entry<Money, Money> bid : book.getBids().entrySet()) {
      if (sideMaxAmount.compareTo(totalAccumulatedAmount) <= 0)
        break;

      if (bucketSize.compareTo(bucketAccumulatedAmount) <= 0) {
        CurrencyPair currencyPair = new CurrencyPair(book.getBaseCurrency().getCode(), book.getCounterCurrency().getCode());
        BigDecimal remainder = bucketAccumulatedAmount.getAmount().remainder(bucketSize.getAmount());
        BigDecimal amount = bucketAccumulatedAmount.minus(remainder).getAmount().min(sideMaxAmount.minus(totalAccumulatedAmount).getAmount());
        Money price = bucketAveragePrice.multipliedBy(bidEdgeMultiplier, RoundingMode.HALF_EVEN).rounded(scale, RoundingMode.FLOOR);

        bidOrders.put(price, new LimitOrder(OrderType.BID, amount, currencyPair, "", new Date(), price.getAmount()));
        totalAccumulatedAmount = totalAccumulatedAmount.plus(amount);
        bucketAccumulatedAmount = Money.of(book.getBaseCurrency(), remainder);
      }

      Money bucketPriceAmount = bucketAveragePrice.multipliedBy(bucketAccumulatedAmount.getAmount(), RoundingMode.HALF_EVEN);
      Money bidPriceAmount = bid.getKey().multipliedBy(bid.getValue().getAmount(), RoundingMode.HALF_EVEN);
      bucketAccumulatedAmount = bucketAccumulatedAmount.plus(bid.getValue());
      bucketAveragePrice = bucketPriceAmount.plus(bidPriceAmount).dividedBy(bucketAccumulatedAmount.getAmount(), RoundingMode.HALF_EVEN);
    }

    ConcurrentNavigableMap<Money, LimitOrder> askOrders = new ConcurrentSkipListMap<Money, LimitOrder>();
    
    bucketAveragePrice = Money.of(book.getCounterCurrency(), BigDecimal.ZERO);
    bucketAccumulatedAmount = Money.of(book.getBaseCurrency(), BigDecimal.ZERO);
    totalAccumulatedAmount = Money.of(book.getBaseCurrency(), BigDecimal.ZERO);

    for (Entry<Money, Money> ask : book.getAsks().entrySet()) {
      if (sideMaxAmount.compareTo(totalAccumulatedAmount) <= 0)
        break;

      if (bucketSize.compareTo(bucketAccumulatedAmount) <= 0) {
        CurrencyPair currencyPair = new CurrencyPair(book.getBaseCurrency().getCode(), book.getCounterCurrency().getCode());
        BigDecimal remainder = bucketAccumulatedAmount.getAmount().remainder(bucketSize.getAmount());
        BigDecimal amount = bucketAccumulatedAmount.minus(remainder).getAmount().min(sideMaxAmount.minus(totalAccumulatedAmount).getAmount());
        Money price = bucketAveragePrice.multipliedBy(askEdgeMultiplier, RoundingMode.HALF_EVEN).rounded(scale, RoundingMode.CEILING);

        askOrders.put(price, new LimitOrder(OrderType.ASK, amount, currencyPair, "", new Date(), price.getAmount()));
        totalAccumulatedAmount = totalAccumulatedAmount.plus(amount);
        bucketAccumulatedAmount = Money.of(book.getBaseCurrency(), remainder);
      }

      Money bucketPriceAmount = bucketAveragePrice.multipliedBy(bucketAccumulatedAmount.getAmount(), RoundingMode.HALF_EVEN);
      Money askPriceAmount = ask.getKey().multipliedBy(ask.getValue().getAmount(), RoundingMode.HALF_EVEN);
      bucketAccumulatedAmount = bucketAccumulatedAmount.plus(ask.getValue());
      bucketAveragePrice = bucketPriceAmount.plus(askPriceAmount).dividedBy(bucketAccumulatedAmount.getAmount(), RoundingMode.HALF_EVEN);
    }

    quoterBidOrders = bidOrders;
    quoterAskOrders = askOrders;
  }

  public class ImpliedBookConsumer implements Runnable {
    public void run() {
      while (true) {
        try {
          ImpliedBitfinexCurrencyPairOrderBook book = impliedBookQueue.take();
          calculateQuotes(book);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
