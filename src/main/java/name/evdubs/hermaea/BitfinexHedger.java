package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.trade.LimitOrder;

public class BitfinexHedger {

  protected final Map<String, BitfinexCurrencyPairOrderBook> orderBooks;
  //protected final List<BlockingQueue<BitfinexCurrencyPairOrderBook>> hedgerOrderBookQueues;
  protected final BlockingQueue<PositionData> positionDataQueue;
  protected final ConcurrentMap<String, PositionData> currencyPairPositions;
  protected final Map<String, BigDecimal> minimumOrderSizes;
  //protected volatile Map<CurrencyUnit, Money> bestExposure;

  public BitfinexHedger(Map<String, BitfinexCurrencyPairOrderBook> orderBooks,
      //List<BlockingQueue<BitfinexCurrencyPairOrderBook>> hedgerOrderBookQueues, 
      BlockingQueue<PositionData> positionDataQueue) {

    this.orderBooks = orderBooks;
    //this.hedgerOrderBookQueues = hedgerOrderBookQueues;
    this.positionDataQueue = positionDataQueue;
    this.currencyPairPositions = new ConcurrentHashMap<String, PositionData>();
    //this.bestExposure = new HashMap<CurrencyUnit, Money>();
    this.minimumOrderSizes = new HashMap<String, BigDecimal>();
    minimumOrderSizes.put("BTCUSD", new BigDecimal("0.01"));
    minimumOrderSizes.put("ETHUSD", new BigDecimal("0.1"));
    minimumOrderSizes.put("ETHBTC", new BigDecimal("0.1"));

    Thread positionConsumerThread = new Thread(new PositionBookConsumer(), PositionBookConsumer.class.getSimpleName());
    positionConsumerThread.start();
    
    /*for (BlockingQueue<BitfinexCurrencyPairOrderBook> bookQueue : hedgerOrderBookQueues) {
      Thread bookQueueThread = new Thread(new OrderBookConsumer(bookQueue), OrderBookConsumer.class.getSimpleName());
      bookQueueThread.start();
    }*/
  }
  
  public ConcurrentMap<String, PositionData> getPositions() {
    return currencyPairPositions;
  }
  
  public List<LimitOrder> getHedgeOrders() {
    
    List<LimitOrder> limitOrders = new ArrayList<LimitOrder>();
    
    System.out.println("Positions are " + currencyPairPositions);
    
    Map<CurrencyUnit, Money> startExposures = computeCurrencyExposure();
    
    System.out.println("Exposed to " + startExposures);
    //System.out.println("Best exposure " + bestExposure);

    if (hasNegativeExposure(startExposures))
      limitOrders = generateHedges(startExposures);
    
    List<LimitOrder> ordersOverLimit = new ArrayList<LimitOrder>();
    for (LimitOrder order : limitOrders) {
      String currencyPair = order.getCurrencyPair().toString().replaceAll("/", "");
      if (currencyPairPositions.containsKey(currencyPair)) {
        PositionData position = currencyPairPositions.get(currencyPair);
        if (LongShort.SHORT.equals(position.getLongShort()) && OrderType.ASK.equals(order.getType())) {
          if (position.getAmount().add(order.getTradableAmount()).compareTo(Hermaea.getPositionLimits().get(currencyPair)) > 0)
            ordersOverLimit.add(order);
        } else if (LongShort.LONG.equals(position.getLongShort()) && OrderType.BID.equals(order.getType())) {
          if (position.getAmount().add(order.getTradableAmount()).compareTo(Hermaea.getPositionLimits().get(currencyPair)) > 0)
            ordersOverLimit.add(order);
        }
      }
    }
    
    if (!ordersOverLimit.isEmpty()) {
      System.out.println("Throwing out all hedge orders due to position limits");
      limitOrders.clear();
    }
    
    Map<CurrencyUnit, Money> endExposures = computeCurrencyExposure();
    
    // if something changed, don't generate potentially bad hedges
    if (!startExposures.equals(endExposures)) {
      System.out.println("Exposure changed since calling getHedgeOrders");
      return new ArrayList<LimitOrder>();
    } else 
      return limitOrders;
  }
  
  protected List<LimitOrder> findProfitableOrders(List<LimitOrder> orderPool, Map<CurrencyUnit, Money> netCurrencyExposure) {
    
    List<List<LimitOrder>> powerList = new ArrayList<List<LimitOrder>>();
    
    for (LimitOrder lo : orderPool) {
      List<List<LimitOrder>> powerListClone = new ArrayList<List<LimitOrder>>(powerList);
      for (List<LimitOrder> list : powerList) {
        List<LimitOrder> listClone = new ArrayList<LimitOrder>(list);
        listClone.add(lo);
        powerListClone.add(listClone);
      }
      
      List<LimitOrder> newSingleList = new ArrayList<LimitOrder>();
      newSingleList.add(lo);
      powerListClone.add(newSingleList);
      
      powerList = powerListClone;
    }
    
    System.out.println("Power list has " + powerList.size() + " elements");
    
    List<List<LimitOrder>> powerListWithoutDupes = new ArrayList<List<LimitOrder>>();

    powerListIter:
    for (List<LimitOrder> limitOrders : powerList) {
      HashSet<CurrencyPair> currencyPairs = new HashSet<CurrencyPair>();
      for (LimitOrder lo : limitOrders) {
        if (!currencyPairs.contains(lo.getCurrencyPair()))
          currencyPairs.add(lo.getCurrencyPair());
        else
          continue powerListIter;
      }
      powerListWithoutDupes.add(limitOrders);
    }
    
    System.out.println("Power list without dupes has " + powerListWithoutDupes.size() + " elements");
    BigDecimal feeMultiplier = new BigDecimal("0.997");
    
    for (List<LimitOrder> limitOrders : powerListWithoutDupes) {
      Map<CurrencyUnit, Money> newExposures = new HashMap<CurrencyUnit, Money>(netCurrencyExposure);
      for (LimitOrder limitOrder : limitOrders) {
        try {
          CurrencyUnit baseCurrency = Hermaea.getBaseCurrencyUnit(limitOrder.getCurrencyPair().toString().replace("/", ""));
          Money baseAmount = Money.of(baseCurrency, limitOrder.getTradableAmount());
          
          CurrencyUnit counterCurrency = Hermaea.getCounterCurrencyUnit(limitOrder.getCurrencyPair().toString().replace("/", ""));
          Money counterAmount = Money.of(counterCurrency, limitOrder.getLimitPrice().multiply(limitOrder.getTradableAmount()), RoundingMode.HALF_EVEN);
          
          if (limitOrder.getType() == OrderType.BID) {
            if (!newExposures.containsKey(baseCurrency))
              newExposures.put(baseCurrency, baseAmount.multipliedBy(feeMultiplier, RoundingMode.HALF_EVEN));
            else
              newExposures.put(baseCurrency, newExposures.get(baseCurrency).plus(baseAmount.multipliedBy(feeMultiplier, RoundingMode.HALF_EVEN)));
            
            if (!newExposures.containsKey(counterCurrency))
              newExposures.put(counterCurrency, counterAmount.negated());
            else
              newExposures.put(counterCurrency, newExposures.get(counterCurrency).minus(counterAmount));
          } else if (limitOrder.getType() == OrderType.ASK) {
            if (!newExposures.containsKey(baseCurrency))
              newExposures.put(baseCurrency, baseAmount.negated());
            else
              newExposures.put(baseCurrency, newExposures.get(baseCurrency).minus(baseAmount));
            
            if (!newExposures.containsKey(counterCurrency))
              newExposures.put(counterCurrency, counterAmount.multipliedBy(feeMultiplier, RoundingMode.HALF_EVEN));
            else
              newExposures.put(counterCurrency, newExposures.get(counterCurrency).plus(counterAmount.multipliedBy(feeMultiplier, RoundingMode.HALF_EVEN)));
          }
        } catch (InvalidCurrencyCodeException e) {
          e.printStackTrace();
        }
      }
      
      boolean hasNegative = false;
      boolean hasOnePositive = false;
      for (Money newExposure : newExposures.values()) {
        /*if (bestExposure.containsKey(newExposure.getCurrencyUnit())) {
          if (bestExposure.get(newExposure.getCurrencyUnit()).compareTo(newExposure) < 0)
            hasOnePositive = true;
          if (bestExposure.get(newExposure.getCurrencyUnit()).compareTo(newExposure) > 0)
            hasNegative = true;
        } else {*/
          if (newExposure.isPositive())
            hasOnePositive = true;
          if (newExposure.isNegative())
            hasNegative = true;
        //}
      }
      
      if (!hasNegative && hasOnePositive) {
        System.out.println("Should have the new exposures " + newExposures);
        //bestExposure = newExposures;
        return limitOrders;
      }
    }
    
    return new ArrayList<LimitOrder>();
  }
  
  protected List<LimitOrder> generateHedges(Map<CurrencyUnit, Money> netCurrencyExposure) {
    
    List<LimitOrder> potentialOrderPool = new ArrayList<LimitOrder>();
    
    // when buying, buy the amount times the fee to make sure we can get back to all long
    BigDecimal bidFeeMultiplier = new BigDecimal("1.00301");
    
    for (BitfinexCurrencyPairOrderBook book : orderBooks.values()) {
      CurrencyUnit baseCurrency = book.getBaseCurrency();
      CurrencyUnit counterCurrency = book.getCounterCurrency();
      
      if (netCurrencyExposure.containsKey(baseCurrency)) {
        if (netCurrencyExposure.get(baseCurrency).isNegative()) {
          Money baseCurrencyExposure = netCurrencyExposure.get(baseCurrency).negated().multipliedBy(bidFeeMultiplier, RoundingMode.HALF_EVEN);
          Money accumulatedAskSize = Money.of(baseCurrency, BigDecimal.ZERO);
          
          for (Entry<Money, Money> ask : book.getForeignAsks().entrySet()) {
            accumulatedAskSize = accumulatedAskSize.plus(ask.getValue());
            if (accumulatedAskSize.isGreaterThan(baseCurrencyExposure)) {
              LimitOrder lo = new LimitOrder(OrderType.BID, baseCurrencyExposure.getAmount(), new CurrencyPair(baseCurrency.getCode(), counterCurrency.getCode()), "", new Date(), ask.getKey().getAmount());
              
              if (lo.getTradableAmount().compareTo(minimumOrderSizes.get(lo.getCurrencyPair().toString().replaceAll("/", ""))) >= 0)
                potentialOrderPool.add(lo);
              
              break;
            }
          }
        } else if (netCurrencyExposure.get(baseCurrency).isPositive()) {
          Money baseCurrencyExposure = netCurrencyExposure.get(baseCurrency);
          Money accumulatedBidSize = Money.of(baseCurrency, BigDecimal.ZERO);
          
          for (Entry<Money, Money> bid : book.getForeignBids().entrySet()) {
            accumulatedBidSize = accumulatedBidSize.plus(bid.getValue());
            if (accumulatedBidSize.isGreaterThan(baseCurrencyExposure)) {
              LimitOrder lo = new LimitOrder(OrderType.ASK, baseCurrencyExposure.getAmount(), new CurrencyPair(baseCurrency.getCode(), counterCurrency.getCode()), "", new Date(), bid.getKey().getAmount());
              
              if (lo.getTradableAmount().compareTo(minimumOrderSizes.get(lo.getCurrencyPair().toString().replaceAll("/", ""))) >= 0)
                potentialOrderPool.add(lo);
              
              break;
            }
          }
        }
      }
      
      if (netCurrencyExposure.containsKey(counterCurrency)) {
        if (netCurrencyExposure.get(counterCurrency).isNegative()) {
          Money counterCurrencyExposure = netCurrencyExposure.get(counterCurrency).negated();
          Money accumulatedBidSize = Money.of(counterCurrency, BigDecimal.ZERO);
          
          for (Entry<Money, Money> bid : book.getForeignBids().entrySet()) {
            accumulatedBidSize = accumulatedBidSize.plus(bid.getKey().multipliedBy(bid.getValue().getAmount(), RoundingMode.HALF_EVEN));
            if (accumulatedBidSize.isGreaterThan(counterCurrencyExposure)) {
              LimitOrder lo = new LimitOrder(OrderType.ASK, counterCurrencyExposure.getAmount().divide(bid.getKey().getAmount(), RoundingMode.HALF_EVEN), new CurrencyPair(baseCurrency.getCode(), counterCurrency.getCode()), "", new Date(), bid.getKey().getAmount());
              
              if (lo.getTradableAmount().compareTo(minimumOrderSizes.get(lo.getCurrencyPair().toString().replaceAll("/", ""))) >= 0)
                potentialOrderPool.add(lo);
              
              break;
            }
          }
        } else if (netCurrencyExposure.get(counterCurrency).isPositive()) {
          Money counterCurrencyExposure = netCurrencyExposure.get(counterCurrency).multipliedBy(bidFeeMultiplier, RoundingMode.HALF_EVEN);
          Money accumulatedAskSize = Money.of(counterCurrency, BigDecimal.ZERO);
          
          for (Entry<Money, Money> ask : book.getForeignAsks().entrySet()) {
            accumulatedAskSize = accumulatedAskSize.plus(ask.getKey().multipliedBy(ask.getValue().getAmount(), RoundingMode.HALF_EVEN));
            if (accumulatedAskSize.isGreaterThan(counterCurrencyExposure)) {
              LimitOrder lo = new LimitOrder(OrderType.BID, counterCurrencyExposure.getAmount().divide(ask.getKey().getAmount(), RoundingMode.HALF_EVEN), new CurrencyPair(baseCurrency.getCode(), counterCurrency.getCode()), "", new Date(), ask.getKey().getAmount());
              
              if (lo.getTradableAmount().compareTo(minimumOrderSizes.get(lo.getCurrencyPair().toString().replaceAll("/", ""))) >= 0)
                potentialOrderPool.add(lo);
              
              break;
            }
          }
        }
      }
    }
    
    System.out.println("Potential order pool has " + potentialOrderPool.size() + " elements");
    
    return findProfitableOrders(potentialOrderPool, netCurrencyExposure);
  }
  
  protected boolean hasNegativeExposure(Map<CurrencyUnit, Money> exposures) {
    
    for (Money money : exposures.values()) {
      if (money.isNegative())
        return true;
    }
    
    return false;
  }
  
  protected Map<CurrencyUnit, Money> computeCurrencyExposure() {
    Map<CurrencyUnit, Money> netCurrencyExposure = new HashMap<CurrencyUnit, Money>();
    for (PositionData positionData : currencyPairPositions.values()) {
      try {
        CurrencyUnit baseCurrency = Hermaea.getBaseCurrencyUnit(positionData.getCurrencyPair());
        Money baseAmount = Money.of(baseCurrency, positionData.getAmount());

        CurrencyUnit counterCurrency = Hermaea.getCounterCurrencyUnit(positionData.getCurrencyPair());
        Money counterAmount = Money.of(counterCurrency, positionData.getAmount().multiply(positionData.getBasePrice()), RoundingMode.HALF_EVEN);

        if (LongShort.LONG.equals(positionData.getLongShort())) {
          if (!netCurrencyExposure.containsKey(baseCurrency))
            netCurrencyExposure.put(baseCurrency, baseAmount);
          else
            netCurrencyExposure.put(baseCurrency, netCurrencyExposure.get(baseCurrency).plus(baseAmount));

          if (!netCurrencyExposure.containsKey(counterCurrency))
            netCurrencyExposure.put(counterCurrency, counterAmount.negated());
          else
            netCurrencyExposure.put(counterCurrency, netCurrencyExposure.get(counterCurrency).minus(counterAmount));
        } else if (LongShort.SHORT.equals(positionData.getLongShort())) {
          if (!netCurrencyExposure.containsKey(baseCurrency))
            netCurrencyExposure.put(baseCurrency, baseAmount.negated());
          else
            netCurrencyExposure.put(baseCurrency, netCurrencyExposure.get(baseCurrency).minus(baseAmount));

          if (!netCurrencyExposure.containsKey(counterCurrency))
            netCurrencyExposure.put(counterCurrency, counterAmount);
          else
            netCurrencyExposure.put(counterCurrency, netCurrencyExposure.get(counterCurrency).plus(counterAmount));
        }
      } catch (InvalidCurrencyCodeException e) {
        e.printStackTrace();
      }
    }
    
    return netCurrencyExposure;
  }

  protected void consumePositionData(PositionData data) {

    if (BigDecimal.ZERO.compareTo(data.getAmount()) == 0 || "CLOSED".equals(data.getStatus()))
      currencyPairPositions.remove(data.getCurrencyPair());
    else 
      currencyPairPositions.put(data.getCurrencyPair(), data);
  }
  
  /*protected class OrderBookConsumer implements Runnable {

    protected BlockingQueue<BitfinexCurrencyPairOrderBook> orderBookQueue;
    
    public OrderBookConsumer(BlockingQueue<BitfinexCurrencyPairOrderBook> orderBookQueue) {
      
      this.orderBookQueue = orderBookQueue;
    }
    
    public void run() {
      
      while (true) {
        try {
          orderBookQueue.take();
          orderBookQueue.clear();

          Map<CurrencyUnit, Money> exposures = computeCurrencyExposure();

          //if (hasNegativeExposure(exposures))
          //  limitOrders = generateHedges(exposures);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    
  }*/

  protected class PositionBookConsumer implements Runnable {

    public void run() {

      while (true) {
        try {
          PositionData data = positionDataQueue.take();

          consumePositionData(data);

          //Map<CurrencyUnit, Money> exposures = computeCurrencyExposure();
          
          //if (hasNegativeExposure(exposures))
          //  limitOrders = generateHedges(exposures);
          
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

  }
}
