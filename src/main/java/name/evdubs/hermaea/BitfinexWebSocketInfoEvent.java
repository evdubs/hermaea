package name.evdubs.hermaea;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BitfinexWebSocketInfoEvent {
  protected final String event;
  protected final String code;
  protected final String version;
  protected final String msg;

  public BitfinexWebSocketInfoEvent(@JsonProperty("event") String event, @JsonProperty("code") String code, @JsonProperty("version") String version,
      @JsonProperty("msg") String msg) {
    this.event = event;
    this.code = code;
    this.version = version;
    this.msg = msg;
  }

  public String getEvent() {
    return event;
  }

  public String getCode() {
    return code;
  }

  public String getVersion() {
    return version;
  }

  public String getMsg() {
    return msg;
  }

  @Override
  public String toString() {
    return "BitfinexWebSocketInfoEvent [event=" + event + ", code=" + code + ", version=" + version + ", msg=" + msg + "]";
  }

}
