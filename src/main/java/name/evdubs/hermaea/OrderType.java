package name.evdubs.hermaea;

public enum OrderType {
  EXCHANGE_FILL_OR_KILL {
    public String toString() {
      
      return "EXCHANGE FOK";
    }
  }, 
  
  EXCHANGE_LIMIT {
    public String toString() {
      
      return "EXCHANGE LIMIT";
    }
  }, 
  
  EXCHANGE_MARKET {
    public String toString() {
      
      return "EXCHANGE MARKET";
    }
  }, 
  
  EXCHANGE_STOP {
    public String toString() {
      
      return "EXCHANGE STOP";
    }
  },
  
  EXCHANGE_TRAILING_STOP {
    public String toString() {
      
      return "EXCHANGE TRAILING STOP";
    }
  },  
  
  FILL_OR_KILL {
    public String toString() {
      
      return "FOK";
    }
  }, 
  
  LIMIT {
    public String toString() {
      
      return "LIMIT";
    }
  }, 
  
  MARKET {
    public String toString() {
      
      return "MARKET";
    }
  }, 
  
  STOP {
    public String toString() {
      
      return "STOP";
    }
  },
  
  TRAILING_STOP {
    public String toString() {
      
      return "TRAILING STOP";
    }
  };
  
  public static OrderType fromString(String s) {
    
    if (EXCHANGE_FILL_OR_KILL.toString().equals(s)) 
      return EXCHANGE_FILL_OR_KILL;
    else if (EXCHANGE_LIMIT.toString().equals(s)) 
      return EXCHANGE_LIMIT;
    else if (EXCHANGE_MARKET.toString().equals(s)) 
      return EXCHANGE_MARKET;
    else if (EXCHANGE_STOP.toString().equals(s)) 
      return EXCHANGE_STOP;
    else if (EXCHANGE_TRAILING_STOP.toString().equals(s)) 
      return EXCHANGE_TRAILING_STOP;
    else if (FILL_OR_KILL.toString().equals(s)) 
      return FILL_OR_KILL;
    else if (LIMIT.toString().equals(s)) 
      return LIMIT;
    else if (MARKET.toString().equals(s)) 
      return MARKET;
    else if (STOP.toString().equals(s)) 
      return STOP;
    else if (TRAILING_STOP.toString().equals(s)) 
      return TRAILING_STOP;
    else 
      return null;
  }
}
