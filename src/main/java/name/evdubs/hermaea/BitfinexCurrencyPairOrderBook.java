package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

public class BitfinexCurrencyPairOrderBook {
  // quote currency "price" -> base currency "amount"
  protected final ConcurrentSkipListMap<Money, Money> fullBidMap;
  protected final ConcurrentSkipListMap<Money, Money> fullAskMap;
  
  protected final ConcurrentSkipListMap<Money, ConcurrentMap<String, OrderData>> authBidOrderMap;
  protected final ConcurrentSkipListMap<Money, ConcurrentMap<String, OrderData>> authAskOrderMap;

  protected final BlockingQueue<CurrencyPairBookDepthData> bookDataQueue;
  protected final List<BlockingQueue<BitfinexCurrencyPairOrderBook>> orderBookQueues;
  
  protected final String currencyPair;
  protected final CurrencyUnit baseCurrency;
  protected final CurrencyUnit counterCurrency;

  public BitfinexCurrencyPairOrderBook(String currencyPair, BlockingQueue<CurrencyPairBookDepthData> bookDataQueue, 
      BlockingQueue<OrderData> orderDataQueue, List<BlockingQueue<BitfinexCurrencyPairOrderBook>> orderBookQueues)
      throws InvalidCurrencyCodeException {
    
    this.fullBidMap = new ConcurrentSkipListMap<Money, Money>(
        new Comparator<Money>() {
          public int compare(Money arg0, Money arg1) {
            // reverse ordering for bids to start at best bid
            return arg1.compareTo(arg0);
          }
        });
    this.fullAskMap = new ConcurrentSkipListMap<Money, Money>();

    this.authBidOrderMap = new ConcurrentSkipListMap<Money, ConcurrentMap<String, OrderData>>(
        new Comparator<Money>() {
          public int compare(Money arg0, Money arg1) {
            // reverse ordering for bids to start at best bid
            return arg1.compareTo(arg0);
          }
        });
    this.authAskOrderMap = new ConcurrentSkipListMap<Money, ConcurrentMap<String, OrderData>>();
    
    this.bookDataQueue = bookDataQueue;
    this.orderBookQueues = orderBookQueues;
    
    this.currencyPair = currencyPair;
    this.baseCurrency = Hermaea.getBaseCurrencyUnit(currencyPair);
    this.counterCurrency = Hermaea.getCounterCurrencyUnit(currencyPair);
    
    Thread depthThread = new Thread(new DepthDataConsumer(this, bookDataQueue), DepthDataConsumer.class.getSimpleName());
    depthThread.start();
    
    Thread orderThread = new Thread(new OrderDataConsumer(this, orderDataQueue), OrderDataConsumer.class.getSimpleName());
    orderThread.start();
  }

  public ConcurrentNavigableMap<Money, Money> getFullBids() {
    return fullBidMap;
  }

  public ConcurrentNavigableMap<Money, Money> getFullAsks() {
    return fullAskMap;
  }
  
  public ConcurrentNavigableMap<Money, ConcurrentMap<String, OrderData>> getAuthBids() {
    return authBidOrderMap;
  }
  
  public ConcurrentNavigableMap<Money, ConcurrentMap<String, OrderData>> getAuthAsks() {
    return authAskOrderMap;
  }
  
  public ConcurrentNavigableMap<Money, Money> getForeignBids() {
    ConcurrentNavigableMap<Money, Money> foreignBids = fullBidMap.clone();
    
    for (Entry<Money, ConcurrentMap<String, OrderData>> priceOrderMapEntry : authBidOrderMap.entrySet()) {
      if (foreignBids.containsKey(priceOrderMapEntry.getKey())) {
        BigDecimal authAmount = BigDecimal.ZERO;
        for (Entry<String, OrderData> orderIdEntry : priceOrderMapEntry.getValue().entrySet()) {
          authAmount = authAmount.add(orderIdEntry.getValue().getAmount());
        }
        
        Money foreignAmount = foreignBids.get(priceOrderMapEntry.getKey()).minus(authAmount);
        if (foreignAmount.isNegativeOrZero())
          foreignBids.remove(priceOrderMapEntry.getKey());
        else
          foreignBids.put(priceOrderMapEntry.getKey(), foreignAmount);
      }
    }
    
    return foreignBids;
  }
  
  public ConcurrentNavigableMap<Money, Money> getForeignAsks() {
    ConcurrentNavigableMap<Money, Money> foreignAsks = fullAskMap.clone();
    
    for (Entry<Money, ConcurrentMap<String, OrderData>> priceOrderMapEntry : authAskOrderMap.entrySet()) {
      if (foreignAsks.containsKey(priceOrderMapEntry.getKey())) {
        BigDecimal authAmount = BigDecimal.ZERO;
        for (Entry<String, OrderData> orderIdEntry : priceOrderMapEntry.getValue().entrySet()) {
          authAmount = authAmount.add(orderIdEntry.getValue().getAmount());
        }
        
        Money foreignAmount = foreignAsks.get(priceOrderMapEntry.getKey()).minus(authAmount);
        if (foreignAmount.isNegativeOrZero())
          foreignAsks.remove(priceOrderMapEntry.getKey());
        else
          foreignAsks.put(priceOrderMapEntry.getKey(), foreignAmount);
      }
    }
    
    return foreignAsks;
  }

  public String getCurrencyPair() {
    return currencyPair;
  }

  public CurrencyUnit getBaseCurrency() {
    return baseCurrency;
  }

  public CurrencyUnit getCounterCurrency() {
    return counterCurrency;
  }

  protected void consumeDepthData(CurrencyPairBookDepthData data) {
    Money updatePrice = Money.of(counterCurrency, data.getPrice());
    Money updateAmount = Money.of(baseCurrency, data.getNewAmount());
    
    if (BidAsk.BID.equals(data.getBidAsk())) {
      if (updateAmount.isZero()) 
        fullBidMap.remove(updatePrice);
      else 
        fullBidMap.put(updatePrice, updateAmount);
    } else if (BidAsk.ASK.equals(data.getBidAsk())) {
      if (updateAmount.isZero()) 
        fullAskMap.remove(updatePrice);
      else 
        fullAskMap.put(updatePrice, updateAmount);
    } 
  }
  
  protected void consumeOrderData(OrderData data) {
    if (OrderType.MARKET.equals(data.getOrderType()) || OrderType.EXCHANGE_MARKET.equals(data.getOrderType()))
      return;
    
    Money updatePrice = Money.of(counterCurrency, data.getPrice());
    
    if (OrderStatus.CANCELED.equals(data.getOrderStatus()) || OrderStatus.EXECUTED.equals(data.getOrderStatus())) {
      if (BidAsk.BID.equals(data.getBidAsk())) {
        if (authBidOrderMap.containsKey(updatePrice)) {
          authBidOrderMap.get(updatePrice).remove(data.getId());
          
          if (authBidOrderMap.get(updatePrice).isEmpty())
            authBidOrderMap.remove(updatePrice); 
        }
      } else if (BidAsk.ASK.equals(data.getBidAsk())) {
        if (authAskOrderMap.containsKey(updatePrice)) {
          authAskOrderMap.get(updatePrice).remove(data.getId());
          
          if (authAskOrderMap.get(updatePrice).isEmpty())
            authAskOrderMap.remove(updatePrice);
        }
      }
    } else {
      if (BidAsk.BID.equals(data.getBidAsk())) {
        if (!authBidOrderMap.containsKey(updatePrice))
          authBidOrderMap.put(updatePrice, new ConcurrentHashMap<String, OrderData>());
        
        authBidOrderMap.get(updatePrice).put(data.getId(), data);
      } else if (BidAsk.ASK.equals(data.getBidAsk())) {
        if (!authAskOrderMap.containsKey(updatePrice))
          authAskOrderMap.put(updatePrice, new ConcurrentHashMap<String, OrderData>());
        
        authAskOrderMap.get(updatePrice).put(data.getId(), data);
      }
    }
  }

  public class DepthDataConsumer implements Runnable {
    protected BitfinexCurrencyPairOrderBook book;
    protected BlockingQueue<CurrencyPairBookDepthData> bookDataQueue;

    public DepthDataConsumer(BitfinexCurrencyPairOrderBook book,
        BlockingQueue<CurrencyPairBookDepthData> bookDataQueue) {
      this.book = book;
      this.bookDataQueue = bookDataQueue;
    }

    public void run() {
      while (true) {
        try {
          CurrencyPairBookDepthData data = bookDataQueue.take();
          book.consumeDepthData(data);

          for (BlockingQueue<BitfinexCurrencyPairOrderBook> queue : orderBookQueues) {
            queue.put(book);
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  protected class OrderDataConsumer implements Runnable {
    protected BitfinexCurrencyPairOrderBook book;
    protected BlockingQueue<OrderData> orderDataQueue;
    
    public OrderDataConsumer(BitfinexCurrencyPairOrderBook book, 
        BlockingQueue<OrderData> orderDataQueue) {
      this.book = book;
      this.orderDataQueue = orderDataQueue;
    }
    
    public void run() {
      while (true) {
        try {
          OrderData data = orderDataQueue.take();
          book.consumeOrderData(data);
          
          for (BlockingQueue<BitfinexCurrencyPairOrderBook> queue : orderBookQueues) {
            queue.put(book);
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
