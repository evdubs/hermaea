package name.evdubs.hermaea;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BitfinexWebSocketAuthEvent {
  protected final String event;
  protected final String status;
  protected final String chanId;
  protected final String userId;
  protected final String code;

  public BitfinexWebSocketAuthEvent(@JsonProperty("event") String event, @JsonProperty("status") String status,
      @JsonProperty("chanId") String chanId, @JsonProperty("userId") String userId, @JsonProperty("code") String code) {
    
    this.event = event;
    this.status = status;
    this.chanId = chanId;
    this.userId = userId;
    this.code = code;
  }

  public String getEvent() {
    
    return event;
  }

  public String getStatus() {
    
    return status;
  }

  public String getChanId() {
    
    return chanId;
  }

  public String getUserId() {
    
    return userId;
  }

  public String getCode() {
    
    return code;
  }

  @Override
  public String toString() {
    
    return "BitfinexWebSocketAuthEvent [event=" + event + ", status=" + status + ", chanId=" + chanId + ", userId=" + userId + ", code=" + code + "]";
  }

}
