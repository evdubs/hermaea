package name.evdubs.hermaea;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.joda.money.Money;

import org.knowm.xchange.bitfinex.v1.BitfinexOrderType;
import org.knowm.xchange.bitfinex.v1.service.polling.BitfinexTradeServiceRaw;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.trade.LimitOrder;

public class BitfinexOrderManager {

  protected final BitfinexTradeServiceRaw tradeService;
  protected final Map<String, ImpliedCurrencyPairQuoter> quoters;
  protected final Map<String, BitfinexCurrencyPairOrderBook> orderBooks;
  protected final BitfinexHedger hedger;
  protected final CopyOnWriteArrayList<LimitOrder> pendingOrders;

  public BitfinexOrderManager(BitfinexTradeServiceRaw tradeService, Map<String, ImpliedCurrencyPairQuoter> quoters,
      Map<String, BitfinexCurrencyPairOrderBook> orderBooks, BitfinexHedger hedger, List<BlockingQueue<OrderData>> orderDataQueues) {

    this.tradeService = tradeService;
    this.quoters = quoters;
    this.orderBooks = orderBooks;
    this.hedger = hedger;
    this.pendingOrders = new CopyOnWriteArrayList<LimitOrder>();
    
    for (BlockingQueue<OrderData> queue : orderDataQueues) {
      Thread t = new Thread(new OrderDataConsumer(queue), OrderDataConsumer.class.getSimpleName());
      t.start();
    }

    Thread t = new Thread(new BitfinexOrderSender(), BitfinexOrderSender.class.getSimpleName());
    t.start();
  }
  
  protected class OrderDataConsumer implements Runnable {
    protected final BlockingQueue<OrderData> queue;
    
    public OrderDataConsumer(BlockingQueue<OrderData> queue) {
      
      this.queue = queue;
    }
    
    public void run() {
      
      while (true) {
        try {
          OrderData data = queue.take();
          
          LimitOrder orderToRemove = null;
          
          for (LimitOrder order : pendingOrders) {
            if (data.satisfiesLimitOrder(order)) {
              orderToRemove = order;
              break;
            }
          }
          
          if (orderToRemove != null)
            pendingOrders.remove(orderToRemove);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  protected class BitfinexOrderSender implements Runnable {

    public void run() {

      while (true) {
        Date startTimestamp = new Date();
        
        Map<String, Map<Money, LimitOrder>> pendingBids = new HashMap<String, Map<Money,LimitOrder>>();
        Map<String, Map<Money, LimitOrder>> pendingAsks = new HashMap<String, Map<Money,LimitOrder>>();
        List<LimitOrder> startHedgerOrders = hedger.getHedgeOrders();
        for (LimitOrder lo : startHedgerOrders) {
          String currencyPair = lo.getCurrencyPair().toString().replaceAll("/", "");
          try {
            Money price = Money.of(Hermaea.getCounterCurrencyUnit(currencyPair), lo.getLimitPrice());
            
            if (OrderType.BID.equals(lo.getType())) {
              if (!pendingBids.containsKey(currencyPair))
                pendingBids.put(currencyPair, new TreeMap<Money, LimitOrder>());
              
              pendingBids.get(currencyPair).put(price, lo);
            } else if (OrderType.ASK.equals(lo.getType())) {
              if (!pendingAsks.containsKey(currencyPair))
                pendingAsks.put(currencyPair, new TreeMap<Money, LimitOrder>());
              
              pendingAsks.get(currencyPair).put(price, lo);
            }
          } catch (InvalidCurrencyCodeException e) {
            e.printStackTrace();
          }
        }
        
        for (Entry<String, ImpliedCurrencyPairQuoter> quoter : quoters.entrySet()) {
          for (Entry<Money, LimitOrder> bid : quoter.getValue().getQuoterBidOrders().entrySet()) {
            if (!pendingBids.containsKey(quoter.getKey()))
              pendingBids.put(quoter.getKey(), new TreeMap<Money, LimitOrder>());
            
            pendingBids.get(quoter.getKey()).put(bid.getKey(), bid.getValue());
          }
          
          for (Entry<Money, LimitOrder> ask : quoter.getValue().getQuoterAskOrders().entrySet()) {
            if (!pendingAsks.containsKey(quoter.getKey()))
              pendingAsks.put(quoter.getKey(), new TreeMap<Money, LimitOrder>());
            
            pendingAsks.get(quoter.getKey()).put(ask.getKey(), ask.getValue());
          }
        }

        List<String> orderIdsToCancel = new ArrayList<String>();
        for (Entry<String, BitfinexCurrencyPairOrderBook> book : orderBooks.entrySet()) {
          for (Entry<Money, ConcurrentMap<String, OrderData>> bidIdMapEntry : book.getValue().getAuthBids().entrySet()) {
            if (bidIdMapEntry.getValue().size() > 1) {
              System.out.println("Found multiple bids at price " + bidIdMapEntry.getKey());
              for (Entry<String, OrderData> idDataEntry : bidIdMapEntry.getValue().entrySet()) {
                orderIdsToCancel.add(idDataEntry.getKey());
              }
            } else {
              for (Entry<String, OrderData> bid : bidIdMapEntry.getValue().entrySet()) {
                if (!pendingBids.containsKey(book.getKey())) {
                  System.out.println("pendingBids doesn't contain " + book.getKey());
                  orderIdsToCancel.add(bid.getValue().getId());
                } else if (!pendingBids.get(book.getKey()).containsKey(bidIdMapEntry.getKey())) {
                  System.out.println(pendingBids.get(book.getKey()).keySet() + " doesn't contain " + bid.getKey());
                  orderIdsToCancel.add(bid.getValue().getId());
                } else if (pendingBids.get(book.getKey()).get(bidIdMapEntry.getKey()).getTradableAmount().compareTo(bid.getValue().getAmount()) != 0) {
                  System.out.println("pendingBids contains " + bidIdMapEntry.getKey() + " but differs on " + bid.getValue().getAmount());
                  orderIdsToCancel.add(bid.getValue().getId());
                }
              }
            }
          }

          for (Entry<Money, ConcurrentMap<String, OrderData>> askIdMapEntry : book.getValue().getAuthAsks().entrySet()) {
            if (askIdMapEntry.getValue().size() > 1) {
              System.out.println("Found multiple asks at price " + askIdMapEntry.getKey());
              for (Entry<String, OrderData> idDataEntry : askIdMapEntry.getValue().entrySet()) {
                orderIdsToCancel.add(idDataEntry.getKey());
              }
            } else {
              for (Entry<String, OrderData> ask : askIdMapEntry.getValue().entrySet()) {
                if (!pendingAsks.containsKey(book.getKey())) {
                  System.out.println("pendingAsks doesn't contain " + book.getKey());
                  orderIdsToCancel.add(ask.getValue().getId());
                } else if (!pendingAsks.get(book.getKey()).containsKey(askIdMapEntry.getKey())) {
                  System.out.println(pendingAsks.get(book.getKey()).keySet() + " doesn't contain " + ask.getKey());
                  orderIdsToCancel.add(ask.getValue().getId());
                } else if (pendingAsks.get(book.getKey()).get(askIdMapEntry.getKey()).getTradableAmount().compareTo(ask.getValue().getAmount()) != 0) {
                  System.out.println("pendingAsks contains " + askIdMapEntry.getKey() + " but differs on " + ask.getValue().getAmount());
                  orderIdsToCancel.add(ask.getValue().getId());
                }
              }
            }
          }
        }

        List<LimitOrder> ordersToSend = new ArrayList<LimitOrder>();
        for (Entry<String, Map<Money, LimitOrder>> pendingBidEntry : pendingBids.entrySet()) {
          for (Entry<Money, LimitOrder> bid : pendingBidEntry.getValue().entrySet()) {
            if (!orderBooks.containsKey(pendingBidEntry.getKey())) {
              ordersToSend.add(bid.getValue());
            } else if (!orderBooks.get(pendingBidEntry.getKey()).getAuthBids().containsKey(bid.getKey())) {
              ordersToSend.add(bid.getValue());
            } else {
              boolean orderActive = false;
              for (Entry<String, OrderData> bookBid : orderBooks.get(pendingBidEntry.getKey()).getAuthBids().get(bid.getKey()).entrySet()) {
                if (bookBid.getValue().getAmount().compareTo(bid.getValue().getTradableAmount()) == 0)
                  orderActive = true;
              }
              
              if (!orderActive)
                ordersToSend.add(bid.getValue());
            }
          }
        }

        for (Entry<String, Map<Money, LimitOrder>> pendingAskEntry : pendingAsks.entrySet()) {
          for (Entry<Money, LimitOrder> ask : pendingAskEntry.getValue().entrySet()) {
            if (!orderBooks.containsKey(pendingAskEntry.getKey())) {
              ordersToSend.add(ask.getValue());
            } else if (!orderBooks.get(pendingAskEntry.getKey()).getAuthAsks().containsKey(ask.getKey())) {
              ordersToSend.add(ask.getValue());
            } else {
              boolean orderActive = false;
              for (Entry<String, OrderData> bookAsk : orderBooks.get(pendingAskEntry.getKey()).getAuthAsks().get(ask.getKey()).entrySet()) {
                if (bookAsk.getValue().getAmount().compareTo(ask.getValue().getTradableAmount()) == 0)
                  orderActive = true;
              }
              
              if (!orderActive)
                ordersToSend.add(ask.getValue());
            }
          }
        }
        
        List<LimitOrder> ordersOverLimit = new ArrayList<LimitOrder>();
        for (LimitOrder order : ordersToSend) {
          String currencyPair = order.getCurrencyPair().toString().replaceAll("/", "");
          if (hedger.getPositions().containsKey(currencyPair)) {
            PositionData position = hedger.getPositions().get(currencyPair);
            if (LongShort.SHORT.equals(position.getLongShort()) && OrderType.ASK.equals(order.getType())) {
              if (position.getAmount().add(order.getTradableAmount()).compareTo(Hermaea.getPositionLimits().get(currencyPair)) > 0)
                ordersOverLimit.add(order);
            } else if (LongShort.LONG.equals(position.getLongShort()) && OrderType.BID.equals(order.getType())) {
              if (position.getAmount().add(order.getTradableAmount()).compareTo(Hermaea.getPositionLimits().get(currencyPair)) > 0)
                ordersOverLimit.add(order);
            }
          }
        }
        
        if (!ordersOverLimit.isEmpty()) {
          System.out.println("Throwing out " + ordersOverLimit.size() + " orders due to position limits");
          ordersToSend.removeAll(ordersOverLimit);
        }

        try {
          // no pending orders - should be safe
          if (pendingOrders.isEmpty()) {
            if (!orderIdsToCancel.isEmpty())
              tradeService.cancelBitfinexOrderMulti(orderIdsToCancel);
            
            if (!ordersToSend.isEmpty()) {
              pendingOrders.addAll(ordersToSend);
              tradeService.placeBitfinexOrderMulti(ordersToSend, BitfinexOrderType.MARGIN_LIMIT);
            }
          }
        } catch (IOException e1) {
          pendingOrders.removeAll(ordersToSend);
          e1.printStackTrace();
        }

        Date endTimestamp = new Date();
        // sleep for a second minus the time taken to cancel and submit orders
        try {
          long duration = Math.max(0l, 2000l - (endTimestamp.getTime() - startTimestamp.getTime()));
          System.out.println("Sleeping for " + duration + " millis");
          Thread.sleep(duration);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
