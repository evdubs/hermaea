package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.knowm.xchange.dto.trade.LimitOrder;

public class OrderData {

  protected final String id;
  protected final String currencyPair;
  protected final BidAsk bidAsk;
  protected final BigDecimal amount;
  protected final BigDecimal originalAmount;
  protected final OrderType orderType;
  protected final OrderStatus orderStatus;
  protected final BigDecimal price;
  protected final BigDecimal averagePrice;
  protected final Date createdDate;
  protected final boolean notify;
  protected final boolean hidden;
  protected final String orderCancelsOtherId;
  
  protected static final SimpleDateFormat sdfMillis = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
  protected static final SimpleDateFormat sdfNoMillis = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

  public OrderData(String id, String currencyPair, BidAsk bidAsk, BigDecimal amount, BigDecimal originalAmount, OrderType orderType,
      OrderStatus orderStatus, BigDecimal price, BigDecimal averagePrice, Date createdDate, boolean notify, boolean hidden, String orderCancelsOtherId) {

    this.id = id;
    this.currencyPair = currencyPair;
    this.bidAsk = bidAsk;
    this.amount = amount;
    this.originalAmount = originalAmount;
    this.orderType = orderType;
    this.orderStatus = orderStatus;
    this.price = price;
    this.averagePrice = averagePrice;
    this.createdDate = createdDate;
    this.notify = notify;
    this.hidden = hidden;
    this.orderCancelsOtherId = orderCancelsOtherId;
  }

  public static OrderData create(String[] array) {

    String id = array[0];
    String currencyPair = array[1];
    BigDecimal amount = new BigDecimal(array[2]);
    BigDecimal originalAmount = new BigDecimal(array[3]);

    BidAsk bidAsk = BidAsk.BID;
    if (amount.compareTo(BigDecimal.ZERO) < 0 || originalAmount.compareTo(BigDecimal.ZERO) < 0)
      bidAsk = BidAsk.ASK;
    
    OrderType orderType = OrderType.fromString(array[4]);
    OrderStatus orderStatus = OrderStatus.fromString(array[5]);
    
    BigDecimal price = null;
    if (array[6] != null)
      price = new BigDecimal(array[6]);
    
    BigDecimal averagePrice = new BigDecimal(array[7]);

    Date createdDate = new Date();
    try {
      if (array[8].contains("."))
        createdDate = sdfMillis.parse(array[8]);
      else 
        createdDate = sdfNoMillis.parse(array[8]);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    boolean notify = false;
    if ("1".equals(array[9]))
      notify = true;

    boolean hidden = false;
    if ("1".equals(array[10]))
      hidden = true;

    String orderCancelsOtherId = array[11];

    return new OrderData(id, currencyPair, bidAsk, amount.abs(), originalAmount.abs(), orderType, orderStatus, price, averagePrice, createdDate,
        notify, hidden, orderCancelsOtherId);
  }
  
  public boolean satisfiesLimitOrder(LimitOrder limitOrder) {
    if (limitOrder.getCurrencyPair().toString().replace("/", "").equals(currencyPair) && 
        limitOrder.getLimitPrice().compareTo(price) == 0 &&
        limitOrder.getTradableAmount().compareTo(originalAmount) == 0 && 
        bidAsk.equalsOrderType(limitOrder.getType()))
      return true;
    
    return false;
  }

  public String getId() {

    return id;
  }

  public String getCurrencyPair() {

    return currencyPair;
  }

  public BidAsk getBidAsk() {

    return bidAsk;
  }

  public BigDecimal getAmount() {

    return amount;
  }

  public BigDecimal getOriginalAmount() {

    return originalAmount;
  }

  public OrderType getOrderType() {

    return orderType;
  }

  public OrderStatus getOrderStatus() {

    return orderStatus;
  }

  public BigDecimal getPrice() {

    return price;
  }

  public BigDecimal getAveragePrice() {

    return averagePrice;
  }

  public Date getCreatedDate() {

    return createdDate;
  }

  public boolean isNotify() {

    return notify;
  }

  public boolean isHidden() {

    return hidden;
  }

  public String getOrderCancelsOtherId() {

    return orderCancelsOtherId;
  }

  @Override
  public String toString() {

    return "OrderData [id=" + id + ", currencyPair=" + currencyPair + ", bidAsk=" + bidAsk + ", amount=" + amount + ", originalAmount="
        + originalAmount + ", orderType=" + orderType + ", orderStatus=" + orderStatus + ", price=" + price + ", averagePrice=" + averagePrice
        + ", createdDate=" + createdDate + ", notify=" + notify + ", hidden=" + hidden + ", orderCancelsOtherId=" + orderCancelsOtherId + "]";
  }

}
