package name.evdubs.hermaea;

public interface DataHandler<T> {
  public void onData(T data);
}
