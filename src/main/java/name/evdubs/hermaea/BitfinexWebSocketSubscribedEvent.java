package name.evdubs.hermaea;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BitfinexWebSocketSubscribedEvent {
  protected final String event;
  protected final String channel;
  protected final String chanId;
  protected final String pair;
  protected final String prec;
  protected final String freq;
  protected final String len;

  public BitfinexWebSocketSubscribedEvent(@JsonProperty("event") String event, @JsonProperty("channel") String channel,
      @JsonProperty("chanId") String chanId, @JsonProperty("pair") String pair, @JsonProperty("prec") String prec, @JsonProperty("freq") String freq,
      @JsonProperty("len") String len) {
    this.event = event;
    this.channel = channel;
    this.chanId = chanId;
    this.pair = pair;
    this.prec = prec;
    this.freq = freq;
    this.len = len;
  }

  public String getEvent() {
    return event;
  }

  public String getChannel() {
    return channel;
  }

  public String getChanId() {
    return chanId;
  }

  public String getPair() {
    return pair;
  }

  public String getPrec() {
    return prec;
  }

  public String getFreq() {
    return freq;
  }

  public String getLen() {
    return len;
  }

  @Override
  public String toString() {
    return "BitfinexWebSocketSubscribedEvent [event=" + event + ", channel=" + channel + ", chanId=" + chanId + ", pair=" + pair + ", prec=" + prec
        + ", freq=" + freq + ", len=" + len + "]";
  }

}
