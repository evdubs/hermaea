package name.evdubs.hermaea;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import javax.crypto.Mac;

import si.mazi.rescu.RestInvocation;
import net.iharder.Base64;

import org.knowm.xchange.service.BaseParamsDigest;

public class BitfinexHmacWebsocketAuthDigest extends BaseParamsDigest {

  protected BitfinexHmacWebsocketAuthDigest(String secretKeyBase64) throws IllegalArgumentException {
    
    super(secretKeyBase64, HMAC_SHA_384);
  }

  public static BitfinexHmacWebsocketAuthDigest createInstance(String secretKeyBase64) {

    return secretKeyBase64 == null ? null : new BitfinexHmacWebsocketAuthDigest(secretKeyBase64);
  }
  
  public String digestStringParams(String payload) {

    Mac mac = getMac();
    try {
      mac.update(payload.getBytes("UTF-8"));
    } catch (IllegalStateException e) {
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    
    return String.format("%096x", new BigInteger(1, mac.doFinal()));
  }

  /**
   * Compatibility method implementation taken from XChange. If you need this method,
   * you should use {@link com.xeiam.xchange.bitfinex.v1.service.BitfinexHmacPostBodyDigest}
   */
  public String digestParams(RestInvocation restInvocation) {

    String postBody = restInvocation.getRequestBody();
    Mac mac = getMac();
    mac.update(Base64.encodeBytes(postBody.getBytes()).getBytes());

    return String.format("%096x", new BigInteger(1, mac.doFinal()));
  }
}
