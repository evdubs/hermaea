package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.util.Date;

public class TradeData {

  protected final String id;
  protected final String currencyPair;
  protected final Date date;
  protected final String orderId;
  protected final BuySell buySell;
  protected final BigDecimal amount;
  protected final BigDecimal price;
  protected final OrderType orderType;
  protected final BigDecimal orderPrice;
  protected final BigDecimal fee;
  protected final String feeCurrency;

  public TradeData(String id, String currencyPair, Date date, String orderId, BuySell buySell, BigDecimal amount, BigDecimal price,
      OrderType orderType, BigDecimal orderPrice, BigDecimal fee, String feeCurrency) {

    this.id = id;
    this.currencyPair = currencyPair;
    this.date = date;
    this.orderId = orderId;
    this.buySell = buySell;
    this.amount = amount;
    this.price = price;
    this.orderType = orderType;
    this.orderPrice = orderPrice;
    this.fee = fee;
    this.feeCurrency = feeCurrency;
  }

  public static TradeData create(String[] array) {

    String id = array[0];
    String currencyPair = array[1];
    Date date = new Date(Long.parseLong(array[2]) * 1000);
    String orderId = array[3];
    BigDecimal amount = new BigDecimal(array[4]);

    BuySell buySell = BuySell.BUY;
    if (amount.compareTo(BigDecimal.ZERO) < 0)
      buySell = BuySell.SELL;

    BigDecimal price = new BigDecimal(array[5]);
    OrderType orderType = OrderType.fromString(array[6]);
    
    BigDecimal orderPrice = null; 
    if (array[7] != null)
      orderPrice = new BigDecimal(array[7]);
    
    BigDecimal fee = null;
    if (array[8] != null)
      fee = new BigDecimal(array[8]);
    
    String feeCurrency = null; 
    if (array[9] != null)
      feeCurrency = array[9];

    return new TradeData(id, currencyPair, date, orderId, buySell, amount.abs(), price, orderType, orderPrice, fee, feeCurrency);
  }

  public String getId() {

    return id;
  }

  public String getCurrencyPair() {

    return currencyPair;
  }

  public Date getDate() {

    return date;
  }

  public String getOrderId() {

    return orderId;
  }

  public BuySell getBuySell() {

    return buySell;
  }

  public BigDecimal getAmount() {

    return amount;
  }

  public BigDecimal getPrice() {

    return price;
  }

  public OrderType getOrderType() {

    return orderType;
  }

  public BigDecimal getOrderPrice() {

    return orderPrice;
  }

  public BigDecimal getFee() {

    return fee;
  }

  public String getFeeCurrency() {

    return feeCurrency;
  }

  @Override
  public String toString() {

    return "TradeData [id=" + id + ", currencyPair=" + currencyPair + ", date=" + date + ", orderId=" + orderId + ", buySell=" + buySell
        + ", amount=" + amount + ", price=" + price + ", orderType=" + orderType + ", orderPrice=" + orderPrice + ", fee=" + fee + ", feeCurrency="
        + feeCurrency + "]";
  }

}
