package name.evdubs.hermaea;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientConnectedData extends BitfinexWebSocketData {
  protected final String connectionId;
  
  public ClientConnectedData(@JsonProperty("connection_id") String connectionId) {
    this.connectionId = connectionId;
  }

  public String getConnectionId() {
    return connectionId;
  }

  @Override
  public String toString() {
    return "ClientConnectedData [connectionId=" + connectionId + "]";
  }
  
}
