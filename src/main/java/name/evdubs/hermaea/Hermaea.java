package name.evdubs.hermaea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.LinkedBlockingDeque;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.DeploymentException;

import org.glassfish.tyrus.client.ClientManager;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bitfinex.v1.BitfinexExchange;
import org.knowm.xchange.bitfinex.v1.service.polling.BitfinexTradeServiceRaw;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.w3c.dom.stylesheets.LinkStyle;

/**
 * Main entry point
 *
 */
public class Hermaea {
  public static final CurrencyUnit USD = CurrencyUnit.registerCurrency(CurrencyUnit.USD.getCurrencyCode(), CurrencyUnit.USD.getNumericCode(), 8,
      new ArrayList<String>(CurrencyUnit.USD.getCountryCodes()), true);
  public static final CurrencyUnit BTC = CurrencyUnit.registerCurrency("BTC", -1, 8, new ArrayList<String>(), true);
  public static final CurrencyUnit ETH = CurrencyUnit.registerCurrency("ETH", -1, 8, new ArrayList<String>(), true);
  public static final CurrencyUnit LTC = CurrencyUnit.registerCurrency("LTC", -1, 8, new ArrayList<String>(), true);
  protected static final Map<String, BigDecimal> positionLimits = new HashMap<String, BigDecimal>();

  public static CurrencyUnit getBaseCurrencyUnit(String currencyPair) throws InvalidCurrencyCodeException {
    String base = currencyPair.substring(0, 3);
    return getCurrencyUnit(base);
  }

  public static CurrencyUnit getCounterCurrencyUnit(String currencyPair) throws InvalidCurrencyCodeException {
    String counter = currencyPair.substring(3, 6);
    return getCurrencyUnit(counter);
  }

  public static CurrencyUnit getCurrencyUnit(String currencyCode) throws InvalidCurrencyCodeException {
    if ("USD".equals(currencyCode))
      return USD;
    else if ("BTC".equals(currencyCode))
      return BTC;
    else if ("ETH".equals(currencyCode))
      return ETH;
    else if ("LTC".equals(currencyCode))
      return LTC;
    else
      throw new InvalidCurrencyCodeException("Could not find currency " + currencyCode);
  }
  
  public static Map<String, BigDecimal> getPositionLimits() {
    return positionLimits;
  }

  public static void printLevels(ConcurrentNavigableMap<Money, ?> bids, ConcurrentNavigableMap<Money, ?> asks, String prefix) {
    int i = 0;
    for (Entry<Money, ?> entry : bids.entrySet()) {
      System.out.print(prefix + " bids ");
      System.out.println(entry);
      i++;
      if (i == 2) {
        i = 0;
        break;
      }
    }

    for (Entry<Money, ?> entry : asks.entrySet()) {
      System.out.print(prefix + " asks ");
      System.out.println(entry);
      i++;
      if (i == 2) {
        i = 0;
        break;
      }
    }
  }
  
  public static void printHedgeOrders(List<LimitOrder> orders) {
    for (LimitOrder order : orders) {
      System.out.println(order);
    }
  }

  public static void main(String[] args) {
    // define position limits
    positionLimits.put("BTCUSD", new BigDecimal("0.1"));
    positionLimits.put("ETHUSD", new BigDecimal("5"));
    positionLimits.put("ETHBTC", new BigDecimal("5"));
    
    // Use the factory to get BFX exchange API using default settings
    Exchange bfx = ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.class.getName());

    ExchangeSpecification bfxSpec = bfx.getDefaultExchangeSpecification();
    
    String apiKey = "";
    String secretKey = "";

    try {
      System.out.print("API Key: ");
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      apiKey = br.readLine();

      System.out.print("Secret Key: ");
      secretKey = br.readLine();

      br.close();
    } catch (IOException e2) {
      System.out.println("Could not read in keys; exiting.");
      e2.printStackTrace();
      System.exit(1);
    }

    bfxSpec.setApiKey(apiKey);
    bfxSpec.setSecretKey(secretKey);

    bfx.applySpecification(bfxSpec);

    // Get the necessary services
    BitfinexTradeServiceRaw tradeService = (BitfinexTradeServiceRaw) bfx.getPollingTradeService();
    
    ClientManager client = ClientManager.createClient();
    ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();

    try {
      Map<String, BlockingQueue<CurrencyPairTradeData>> tradeDataQueues = new HashMap<String, BlockingQueue<CurrencyPairTradeData>>();
      // depthDataHandlers.add(new DepthDataDbWriter());
      // tradeDataHandlers.add(new TradeDataDbWriter());

      // queues of updated order books to be consumed by the implied order book
      BlockingQueue<BitfinexCurrencyPairOrderBook> btcUsdImpliedQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
      BlockingQueue<BitfinexCurrencyPairOrderBook> ethUsdImpliedQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
      BlockingQueue<BitfinexCurrencyPairOrderBook> ethBtcImpliedQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();

      // queues of updated order books to be consumed by the hedger
      /*BlockingQueue<BitfinexCurrencyPairOrderBook> btcUsdHedgerQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
      BlockingQueue<BitfinexCurrencyPairOrderBook> ethUsdHedgerQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();
      BlockingQueue<BitfinexCurrencyPairOrderBook> ethBtcHedgerQueue = new LinkedBlockingDeque<BitfinexCurrencyPairOrderBook>();*/

      List<BlockingQueue<BitfinexCurrencyPairOrderBook>> btcUsdOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
      btcUsdOrderBookQueues.add(ethBtcImpliedQueue);
      btcUsdOrderBookQueues.add(ethUsdImpliedQueue);
      //btcUsdOrderBookQueues.add(btcUsdHedgerQueue);
      List<BlockingQueue<BitfinexCurrencyPairOrderBook>> ethUsdOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
      ethUsdOrderBookQueues.add(ethBtcImpliedQueue);
      ethUsdOrderBookQueues.add(btcUsdImpliedQueue);
      //ethUsdOrderBookQueues.add(ethUsdHedgerQueue);
      List<BlockingQueue<BitfinexCurrencyPairOrderBook>> ethBtcOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
      ethBtcOrderBookQueues.add(btcUsdImpliedQueue);
      ethBtcOrderBookQueues.add(ethUsdImpliedQueue);
      //ethBtcOrderBookQueues.add(ethBtcHedgerQueue);

      BlockingQueue<CurrencyPairBookDepthData> btcUsdDepthDataQueue = new LinkedBlockingDeque<CurrencyPairBookDepthData>();
      BlockingQueue<CurrencyPairBookDepthData> ethUsdDepthDataQueue = new LinkedBlockingDeque<CurrencyPairBookDepthData>();
      BlockingQueue<CurrencyPairBookDepthData> ethBtcDepthDataQueue = new LinkedBlockingDeque<CurrencyPairBookDepthData>();

      BlockingQueue<OrderData> btcUsdBookOrderDataQueue = new LinkedBlockingDeque<OrderData>();
      BlockingQueue<OrderData> ethUsdBookOrderDataQueue = new LinkedBlockingDeque<OrderData>();
      BlockingQueue<OrderData> ethBtcBookOrderDataQueue = new LinkedBlockingDeque<OrderData>();

      BitfinexCurrencyPairOrderBook btcUsd = new BitfinexCurrencyPairOrderBook("BTCUSD", btcUsdDepthDataQueue, btcUsdBookOrderDataQueue,
          btcUsdOrderBookQueues);
      BitfinexCurrencyPairOrderBook ethUsd = new BitfinexCurrencyPairOrderBook("ETHUSD", ethUsdDepthDataQueue, ethUsdBookOrderDataQueue,
          ethUsdOrderBookQueues);
      BitfinexCurrencyPairOrderBook ethBtc = new BitfinexCurrencyPairOrderBook("ETHBTC", ethBtcDepthDataQueue, ethBtcBookOrderDataQueue,
          ethBtcOrderBookQueues);

      // queues of updated implied books to be consumed by the quoter
      BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> btcUsdQuoterQueue = new LinkedBlockingDeque<ImpliedBitfinexCurrencyPairOrderBook>();
      BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> ethUsdQuoterQueue = new LinkedBlockingDeque<ImpliedBitfinexCurrencyPairOrderBook>();
      BlockingQueue<ImpliedBitfinexCurrencyPairOrderBook> ethBtcQuoterQueue = new LinkedBlockingDeque<ImpliedBitfinexCurrencyPairOrderBook>();

      ImpliedBitfinexCurrencyPairOrderBook impliedBtcUsd = new ImpliedBitfinexCurrencyPairOrderBook(BTC, USD, btcUsdImpliedQueue, btcUsdQuoterQueue);
      ImpliedBitfinexCurrencyPairOrderBook impliedEthUsd = new ImpliedBitfinexCurrencyPairOrderBook(ETH, USD, ethUsdImpliedQueue, ethUsdQuoterQueue);
      ImpliedBitfinexCurrencyPairOrderBook impliedEthBtc = new ImpliedBitfinexCurrencyPairOrderBook(ETH, BTC, ethBtcImpliedQueue, ethBtcQuoterQueue);

      Money twoTenthsBtc = Money.of(BTC, new BigDecimal("0.1"));
      Money sixTenthsBtc = Money.of(BTC, new BigDecimal("0.1"));
      Money tenEth = Money.of(ETH, new BigDecimal("5"));
      Money thirtyEth = Money.of(ETH, new BigDecimal("5"));

      ImpliedCurrencyPairQuoter impliedBtcUsdQuoter = new ImpliedCurrencyPairQuoter(twoTenthsBtc, sixTenthsBtc, btcUsdQuoterQueue);
      ImpliedCurrencyPairQuoter impliedEthUsdQuoter = new ImpliedCurrencyPairQuoter(tenEth, thirtyEth, ethUsdQuoterQueue);
      ImpliedCurrencyPairQuoter impliedEthBtcQuoter = new ImpliedCurrencyPairQuoter(tenEth, thirtyEth, ethBtcQuoterQueue);

      Map<String, BlockingQueue<CurrencyPairBookDepthData>> depthDataQueues = new HashMap<String, BlockingQueue<CurrencyPairBookDepthData>>();
      depthDataQueues.put("BTCUSD", btcUsdDepthDataQueue);
      depthDataQueues.put("ETHUSD", ethUsdDepthDataQueue);
      depthDataQueues.put("ETHBTC", ethBtcDepthDataQueue);
      
      // order data queues to be consumed by the order manager
      BlockingQueue<OrderData> btcUsdManagerOrderDataQueue = new LinkedBlockingDeque<OrderData>();
      BlockingQueue<OrderData> ethUsdManagerOrderDataQueue = new LinkedBlockingDeque<OrderData>();
      BlockingQueue<OrderData> ethBtcManagerOrderDataQueue = new LinkedBlockingDeque<OrderData>();
      
      List<BlockingQueue<OrderData>> btcUsdOrderDataQueues = new ArrayList<BlockingQueue<OrderData>>();
      btcUsdOrderDataQueues.add(btcUsdBookOrderDataQueue);
      btcUsdOrderDataQueues.add(btcUsdManagerOrderDataQueue);
      List<BlockingQueue<OrderData>> ethUsdOrderDataQueues = new ArrayList<BlockingQueue<OrderData>>();
      ethUsdOrderDataQueues.add(ethUsdBookOrderDataQueue);
      ethUsdOrderDataQueues.add(ethUsdManagerOrderDataQueue);
      List<BlockingQueue<OrderData>> ethBtcOrderDataQueues = new ArrayList<BlockingQueue<OrderData>>();
      ethBtcOrderDataQueues.add(ethBtcBookOrderDataQueue);
      ethBtcOrderDataQueues.add(ethBtcManagerOrderDataQueue);

      Map<String, List<BlockingQueue<OrderData>>> orderDataQueues = new HashMap<String, List<BlockingQueue<OrderData>>>();
      orderDataQueues.put("BTCUSD", btcUsdOrderDataQueues);
      orderDataQueues.put("ETHUSD", ethUsdOrderDataQueues);
      orderDataQueues.put("ETHBTC", ethBtcOrderDataQueues);

      BlockingQueue<PositionData> positionDataQueue = new LinkedBlockingDeque<PositionData>();
      
      BitfinexWebSocketSecureClient bfxClient = new BitfinexWebSocketSecureClient(apiKey, secretKey, depthDataQueues, orderDataQueues,
          tradeDataQueues, positionDataQueue);
      client.connectToServer(bfxClient, cec, new URI("wss://api2.bitfinex.com:3000/ws"));
      
      Map<String, BitfinexCurrencyPairOrderBook> orderBooks = new HashMap<String, BitfinexCurrencyPairOrderBook>();
      orderBooks.put("BTCUSD", btcUsd);
      orderBooks.put("ETHUSD", ethUsd);
      orderBooks.put("ETHBTC", ethBtc);
      
      /*List<BlockingQueue<BitfinexCurrencyPairOrderBook>> hedgerOrderBookQueues = new ArrayList<BlockingQueue<BitfinexCurrencyPairOrderBook>>();
      hedgerOrderBookQueues.add(btcUsdHedgerQueue);
      hedgerOrderBookQueues.add(ethUsdHedgerQueue);
      hedgerOrderBookQueues.add(ethBtcHedgerQueue);*/
      
      //BitfinexHedger hedger = new BitfinexHedger(orderBooks, hedgerOrderBookQueues, positionDataQueue);
      BitfinexHedger hedger = new BitfinexHedger(orderBooks, positionDataQueue);
      
      Map<String, ImpliedCurrencyPairQuoter> quoters = new HashMap<String, ImpliedCurrencyPairQuoter>();
      quoters.put("BTCUSD", impliedBtcUsdQuoter);
      quoters.put("ETHUSD", impliedEthUsdQuoter);
      quoters.put("ETHBTC", impliedEthBtcQuoter);

      List<BlockingQueue<OrderData>> managerOrderDataQueues = new ArrayList<BlockingQueue<OrderData>>();
      managerOrderDataQueues.add(btcUsdManagerOrderDataQueue);
      managerOrderDataQueues.add(ethUsdManagerOrderDataQueue);
      managerOrderDataQueues.add(ethBtcManagerOrderDataQueue);
      
      BitfinexOrderManager om = new BitfinexOrderManager(tradeService, quoters, orderBooks, hedger, managerOrderDataQueues);

      while (true) {
        try {
          Thread.sleep(10000);
          printLevels(btcUsd.getFullBids(), btcUsd.getFullAsks(), "btcusd book");
          printLevels(impliedBtcUsd.getBids(), impliedBtcUsd.getAsks(), "btcusd implied");
          printLevels(impliedBtcUsdQuoter.getQuoterBidOrders(), impliedBtcUsdQuoter.getQuoterAskOrders(), "btcusd quotes");
          
          printLevels(ethUsd.getFullBids(), ethUsd.getFullAsks(), "ethusd book");
          printLevels(impliedEthUsd.getBids(), impliedEthUsd.getAsks(), "ethusd implied");
          printLevels(impliedEthUsdQuoter.getQuoterBidOrders(), impliedEthUsdQuoter.getQuoterAskOrders(), "ethusd quotes");
          
          printLevels(ethBtc.getFullBids(), ethBtc.getFullAsks(), "ethbtc book");
          printLevels(impliedEthBtc.getBids(), impliedEthBtc.getAsks(), "ethbtc implied");
          printLevels(impliedEthBtcQuoter.getQuoterBidOrders(), impliedEthBtcQuoter.getQuoterAskOrders(), "ethbtc quotes");
          
          Date start = new Date();
          printHedgeOrders(hedger.getHedgeOrders());
          Date end = new Date();
          System.out.println("Hedger took " + (end.getTime() - start.getTime()) + " millis");
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    } catch (DeploymentException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (InvalidCurrencyCodeException e) {
      e.printStackTrace();
    }
  }
}
