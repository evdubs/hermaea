package name.evdubs.hermaea;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BitfinexWebSocketSecureClient extends Endpoint {
  
  protected String apiKey;
  protected String secretKey;
  protected Map<String, BlockingQueue<CurrencyPairBookDepthData>> depthDataQueues;
  protected Map<String, List<BlockingQueue<OrderData>>> orderDataQueues;
  protected Map<String, BlockingQueue<CurrencyPairTradeData>> tradeDataQueues;
  protected BlockingQueue<PositionData> positionDataQueue;
  protected HashMap<String, String> chanIdPairMap;

  public BitfinexWebSocketSecureClient(String apiKey, String secretKey, 
      Map<String, BlockingQueue<CurrencyPairBookDepthData>> depthDataQueues,
      Map<String, List<BlockingQueue<OrderData>>> orderDataQueues, 
      Map<String, BlockingQueue<CurrencyPairTradeData>> tradeDataQueues,
      BlockingQueue<PositionData> positionDataQueue) {

    this.apiKey = apiKey;
    this.secretKey = secretKey;
    this.depthDataQueues = depthDataQueues;
    this.orderDataQueues = orderDataQueues;
    this.tradeDataQueues = tradeDataQueues;
    this.positionDataQueue = positionDataQueue;
    this.chanIdPairMap = new HashMap<String, String>();
  }

  @OnOpen
  public void onOpen(final Session session, EndpointConfig config) {
    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    final SubscriptionManager manager = new SubscriptionManager(session, depthDataQueues.keySet());
    scheduler.scheduleAtFixedRate(manager, 1, 1, TimeUnit.SECONDS);

    final ObjectMapper om = new ObjectMapper();
    
    final Pattern positionSnapshotPattern = Pattern.compile("^\\[0,\"ps\",(\\[.*?)\\]$");
    final Pattern positionUpdatePattern = Pattern.compile("^\\[0,\"p[nuc]\",(\\[.*?)\\]$");
    final Pattern walletSnapshotPattern = Pattern.compile("^\\[0,\"ws\",(\\[.*?)\\]$");
    final Pattern walletUpdatePattern = Pattern.compile("^\\[0,\"wu\",(\\[.*?)\\]$");
    final Pattern orderSnapshotPattern = Pattern.compile("^\\[0,\"h?os\",(\\[.*?)\\]$");
    final Pattern orderUpdatePattern = Pattern.compile("^\\[0,\"o[nuc]\",(\\[.*?)\\]$");
    final Pattern tradeSnapshotPattern = Pattern.compile("^\\[0,\"ts\",(\\[.*?)\\]$");
    final Pattern tradeUpdatePattern = Pattern.compile("^\\[0,\"tu\",(\\[.*?)\\]$");
    final Pattern orderBookSnapshotPattern = Pattern.compile("^\\[([0-9]+),(\\[\\[.*?)\\]$");
    final Pattern orderBookUpdatePattern = Pattern.compile("^\\[[0-9]+,[0-9]+.*?$");

    session.addMessageHandler(new MessageHandler.Whole<String>() {
      public void onMessage(String message) {
        manager.setPing(new Date());

        try {
          // System.out.println(message);
          if (message.startsWith("[")) {
            // data type
            Matcher positionSnapshotMatcher = positionSnapshotPattern.matcher(message);
            if (positionSnapshotMatcher.matches()) {
              String[][] snapshots = om.readValue(positionSnapshotMatcher.group(1), String[][].class);
              
              for (String[] snapshot : snapshots) {
                PositionData data = PositionData.create(snapshot);
                
                System.out.println(data);
                try {
                  positionDataQueue.put(data);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
            
            Matcher positionUpdateMatcher = positionUpdatePattern.matcher(message);
            if (positionUpdateMatcher.matches()) {
              String[] update = om.readValue(positionUpdateMatcher.group(1), String[].class);
              
              PositionData data = PositionData.create(update);
              
              System.out.println(data);
              try {
                positionDataQueue.put(data);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            
            Matcher walletSnapshotMatcher = walletSnapshotPattern.matcher(message);
            if (walletSnapshotMatcher.matches()) {
              String[][] snapshots = om.readValue(walletSnapshotMatcher.group(1), String[][].class);
              
              for (String[] snapshot : snapshots) {
                WalletData data = WalletData.create(snapshot);
                
                System.out.println(data);
              }
            }
            
            Matcher walletUpdateMatcher = walletUpdatePattern.matcher(message);
            if (walletUpdateMatcher.matches()) {
              String[] update = om.readValue(walletUpdateMatcher.group(1), String[].class);
              
              WalletData data = WalletData.create(update);
              
              System.out.println(data);
            }
            
            Matcher orderSnapshotMatcher = orderSnapshotPattern.matcher(message);
            if (orderSnapshotMatcher.matches()) {
              System.out.println(message);
              
              String[][] snapshots = om.readValue(orderSnapshotMatcher.group(1), String[][].class);
              
              for (String[] snapshot : snapshots) {
                OrderData data = OrderData.create(snapshot);
                
                System.out.println(data);
                
                if (orderDataQueues.containsKey(data.getCurrencyPair())) {
                  try {
                    for (BlockingQueue<OrderData> queue : orderDataQueues.get(data.getCurrencyPair())) {
                      queue.put(data);
                    }
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
              }
            }
            
            Matcher orderUpdateMatcher = orderUpdatePattern.matcher(message);
            if (orderUpdateMatcher.matches()) {
              String[] update = om.readValue(orderUpdateMatcher.group(1), String[].class);
              
              OrderData data = OrderData.create(update);
              
              System.out.println(data);
              
              if (orderDataQueues.containsKey(data.getCurrencyPair())) {
                try {
                  for (BlockingQueue<OrderData> queue : orderDataQueues.get(data.getCurrencyPair())) {
                    queue.put(data);
                  }
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
            
            Matcher tradeSnapshotMatcher = tradeSnapshotPattern.matcher(message);
            if (tradeSnapshotMatcher.matches()) {
              String[][] snapshots = om.readValue(tradeSnapshotMatcher.group(1), String[][].class);
              
              for (String[] snapshot : snapshots) {
                TradeData data = TradeData.create(snapshot);
                
                System.out.println(data);
              }
            }
            
            Matcher tradeUpdateMatcher = tradeUpdatePattern.matcher(message);
            if (tradeUpdateMatcher.matches()) {
              String[] update = om.readValue(tradeUpdateMatcher.group(1), String[].class);
              
              // ignore the trade sequence part of the message (first field)
              TradeData data = TradeData.create(Arrays.copyOfRange(update, 1, update.length));
              
              System.out.println(data);
            }
            
            if (orderBookUpdatePattern.matcher(message).matches()) {
              String[] update = om.readValue(message, String[].class);
              
              CurrencyPairBookDepthData data = CurrencyPairBookDepthData.createFromUpdate(update, chanIdPairMap);
              
              if (depthDataQueues.containsKey(data.getCurrencyPair())) {
                try {
                  depthDataQueues.get(data.getCurrencyPair()).put(data);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
              
              return;
            }
            
            Matcher snapshotMatcher = orderBookSnapshotPattern.matcher(message);
            if (snapshotMatcher.matches()) {
              String chanId = snapshotMatcher.group(1);
              String[][] snapshots = om.readValue(snapshotMatcher.group(2), String[][].class);
              
              for (String[] snapshot : snapshots) {
                CurrencyPairBookDepthData data = CurrencyPairBookDepthData.createFromSnapshot(snapshot, chanIdPairMap.get(chanId));
                
                if (depthDataQueues.containsKey(data.getCurrencyPair())) {
                  try {
                    depthDataQueues.get(data.getCurrencyPair()).put(data);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
              }
              return;
            }
            // System.out.println(message);
          } else if (message.startsWith("{")) {
            // event type - should perhaps be optimized to not have to look through the whole string
            if (message.contains("\"event\":\"auth\"")) {
              BitfinexWebSocketAuthEvent event = om.readValue(message, BitfinexWebSocketAuthEvent.class);
              System.out.println(event);
            } else if (message.contains("\"event\":\"info\"")) {
              BitfinexWebSocketInfoEvent event = om.readValue(message, BitfinexWebSocketInfoEvent.class);
              System.out.println(event);
            } else if (message.contains("\"event\":\"error\"")) {
              BitfinexWebSocketErrorEvent event = om.readValue(message, BitfinexWebSocketErrorEvent.class);
              System.out.println(event);
            } else if (message.contains("\"event\":\"subscribed\"")) {
              BitfinexWebSocketSubscribedEvent event = om.readValue(message, BitfinexWebSocketSubscribedEvent.class);
              chanIdPairMap.put(event.getChanId(), event.getPair());
              System.out.println(event);
            } else if (message.contains("\"event\":\"unsubscribed\"")) {
              BitfinexWebSocketUnsubscribedEvent event = om.readValue(message, BitfinexWebSocketUnsubscribedEvent.class);
              System.out.println(event);
            } else {
              System.out.println("Unknown event message " + message);
            }
          } else {
            System.out.println("Unknown message " + message);
          }
        } catch (JsonParseException e) {
          e.printStackTrace();
        } catch (JsonMappingException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
  }

  @OnMessage
  public void onMessage(Session session, String message) {
    System.out.println(message);
  }

  @OnClose
  public void onClose(Session session, CloseReason closeReason) {
    System.out.println(closeReason);
  }

  @OnError
  public void onError(Session session, Throwable thr) {
    System.out.println(thr);
  }

  public class SubscriptionManager implements Runnable {
    protected Session session;
    protected Set<String> subscriptions;
    protected Date latestPing;
    protected boolean subscribed;

    public SubscriptionManager(Session session, Set<String> subscriptions) {
      this.session = session;
      this.subscriptions = subscriptions;
      latestPing = new Date();
      subscribed = false;
    }

    public void setPing(Date latestPing) {
      this.latestPing = latestPing;
    }

    public void run() {
      Date currentTimestamp = new Date();
      /*
      if (currentTimestamp.getTime() > latestPing.getTime() + 10000) {
        System.out.println("Stale ping; unsubscribing");
        for (String subscription : subscriptions) {
          try {
            session.getBasicRemote().sendText("{\"event\":\"subscribe\",\"channel\":\"" + subscription + "\"}");
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
        subscribed = false;
      }*/

      if (subscribed == false) {
          try {
            String payload = "AUTH" + (new Date()).getTime();
            BitfinexHmacWebsocketAuthDigest digest = BitfinexHmacWebsocketAuthDigest.createInstance(secretKey);
            String sig = digest.digestStringParams(payload);
            String text = "{\"event\":\"auth\",\"apiKey\":\"" + apiKey + "\",\"authSig\":\"" + sig + "\",\"authPayload\":\"" + payload + "\"}";
            System.out.println(text);
            session.getBasicRemote().sendText(text);
            

            for (String subscription : subscriptions) {
              session.getBasicRemote().sendText("{\"event\":\"subscribe\",\"channel\":\"book\",\"pair\":\"" + subscription + "\",\"prec\":\"P0\",\"len\":\"100\"}");
            }
          } catch (IOException e) {
            e.printStackTrace();
          }

        subscribed = true;
      }
    }
  }
}