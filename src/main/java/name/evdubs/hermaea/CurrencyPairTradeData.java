package name.evdubs.hermaea;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrencyPairTradeData extends BitfinexWebSocketData {
  protected final String currencyPair;
  protected final BigDecimal price;
  protected final BigDecimal amount;
  protected final int side;
  protected final double timestamp;

  public CurrencyPairTradeData(@JsonProperty("pair") String pair, @JsonProperty("price") BigDecimal price, @JsonProperty("amount") BigDecimal amount, @JsonProperty("side") int side,
      @JsonProperty("timestamp") double timestamp) {
    this.currencyPair = pair;
    this.price = price;
    this.amount = amount;
    this.side = side;
    this.timestamp = timestamp;
  }

  public String getCurrencyPair() {
    return currencyPair;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public int getSide() {
    return side;
  }

  public double getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return "CurrencyPairTradeData [currencyPair=" + currencyPair + ", price=" + price + ", amount=" + amount + ", side=" + side + ", timestamp=" + timestamp + "]";
  }
  
}
