package name.evdubs.hermaea;

import java.math.BigDecimal;

public class PositionData {
  
  protected final String currencyPair;
  protected final String status;
  protected final BigDecimal amount;
  protected final LongShort longShort;
  protected final BigDecimal basePrice;
  protected final BigDecimal marginAmount;
  protected final MarginFundingSchedule schedule;
  
  public PositionData(String currencyPair, String status, BigDecimal amount, LongShort longShort, BigDecimal basePrice, BigDecimal marginAmount,
      MarginFundingSchedule schedule) {
    
    this.currencyPair = currencyPair;
    this.status = status;
    this.amount = amount;
    this.longShort = longShort;
    this.basePrice = basePrice;
    this.marginAmount = marginAmount;
    this.schedule = schedule;
  }
  
  public static PositionData create(String[] array) {
    
    String currencyPair = array[0];
    String status = array[1];
    BigDecimal amount = new BigDecimal(array[2]);
    
    LongShort longShort = LongShort.LONG;
    if (amount.compareTo(BigDecimal.ZERO) < 0)
      longShort = LongShort.SHORT;
    
    BigDecimal basePrice = new BigDecimal(array[3]);
    BigDecimal marginAmount = new BigDecimal(array[4]);
    
    MarginFundingSchedule schedule = MarginFundingSchedule.DAILY;
    if (array[5].equals("1"))
      schedule = MarginFundingSchedule.TERM;
    
    return new PositionData(currencyPair, status, amount.abs(), longShort, basePrice, marginAmount, schedule);
  }
  
  public String getCurrencyPair() {
    
    return currencyPair;
  }

  public String getStatus() {
    
    return status;
  }

  public BigDecimal getAmount() {
    
    return amount;
  }

  public LongShort getLongShort() {
    
    return longShort;
  }

  public BigDecimal getBasePrice() {
    
    return basePrice;
  }

  public BigDecimal getMarginAmount() {
    
    return marginAmount;
  }

  public MarginFundingSchedule getMarginFundingSchedule() {
    
    return schedule;
  }

  @Override
  public String toString() {
    
    return "PositionData [currencyPair=" + currencyPair + ", status=" + status + ", amount=" + amount + ", longShort=" + longShort + ", basePrice="
        + basePrice + ", marginAmount=" + marginAmount + ", schedule=" + schedule + "]";
  }
  
}
