package name.evdubs.hermaea;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BitfinexWebSocketErrorEvent {
  protected final String event;
  protected final String msg;
  protected final String code;
  protected final String channel;

  public BitfinexWebSocketErrorEvent(@JsonProperty("event") String event, @JsonProperty("msg") String msg, @JsonProperty("code") String code,
      @JsonProperty("channel") String channel) {
    this.event = event;
    this.msg = msg;
    this.code = code;
    this.channel = channel;
  }

  public String getEvent() {
    return event;
  }

  public String getMsg() {
    return msg;
  }

  public String getCode() {
    return code;
  }

  public String getChannel() {
    return channel;
  }

  @Override
  public String toString() {
    return "BitfinexWebSocketErrorEvent [event=" + event + ", msg=" + msg + ", code=" + code + ", channel=" + channel + "]";
  }

}
