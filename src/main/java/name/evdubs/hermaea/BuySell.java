package name.evdubs.hermaea;

public enum BuySell {

  BUY {
    public String toString() {
      
      return "BUY";
    }
  }, 
  
  SELL {
    public String toString() {
      
      return "SELL";
    }
  };
  
  public static BuySell fromString(String s) {
    
    if (BUY.toString().equals(s))
      return BUY;
    else if (SELL.toString().equals(s))
      return SELL;
    else 
      return null;
  }
}
