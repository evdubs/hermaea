package name.evdubs.hermaea;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BitfinexWebSocketUnsubscribedEvent {
  protected final String event;
  protected final String status;
  protected final String chanId;

  public BitfinexWebSocketUnsubscribedEvent(@JsonProperty("event") String event, @JsonProperty("status") String status,
      @JsonProperty("chanId") String chanId) {
    this.event = event;
    this.status = status;
    this.chanId = chanId;
  }

  public String getEvent() {
    return event;
  }

  public String getStatus() {
    return status;
  }

  public String getChanId() {
    return chanId;
  }

  @Override
  public String toString() {
    return "BitfinexWebSocketUnsubscribedEvent [event=" + event + ", status=" + status + ", chanId=" + chanId + "]";
  }

}
