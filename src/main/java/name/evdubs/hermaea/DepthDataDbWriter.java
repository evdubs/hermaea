package name.evdubs.hermaea;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DepthDataDbWriter implements DataHandler<CurrencyPairBookDepthData> {
  private BlockingQueue<CurrencyPairBookDepthData> depthQueue;

  public DepthDataDbWriter() {
    depthQueue = new LinkedBlockingQueue<CurrencyPairBookDepthData>();
    DepthDataDbWriterRunnable depthRunnable = new DepthDataDbWriterRunnable(
        depthQueue);

    (new Thread(depthRunnable)).start();
  }

  public void onData(CurrencyPairBookDepthData data) {
    try {
      depthQueue.put(data);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private class DepthDataDbWriterRunnable implements Runnable {
    private BlockingQueue<CurrencyPairBookDepthData> depthQueue;

    public DepthDataDbWriterRunnable(
        BlockingQueue<CurrencyPairBookDepthData> depthQueue) {
      this.depthQueue = depthQueue;
    }

    public void run() {
      PreparedStatement ps;
      try {
        Connection c = DriverManager.getConnection("jdbc:postgresql://10.0.0.3:5432/", "postgres", "");
        ps = c.prepareStatement("insert into pair_depth_data "
            + "values(?::text, ?::numeric, ?::numeric, ?::timestamp)");
      } catch (SQLException e1) {
        e1.printStackTrace();
        return;
      }
      
      while (true) {
        try {
          CurrencyPairBookDepthData data = depthQueue.take();

          ps.setString(1, data.getCurrencyPair());
          ps.setBigDecimal(2, data.getPrice());
          ps.setBigDecimal(3, data.getNewAmount());
          ps.setDate(4, new java.sql.Date(data.getTimestamp().getTime()));
          
          ps.execute();
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
