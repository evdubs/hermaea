package name.evdubs.hermaea;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class CurrencyPairBookDepthData {
  protected final String currencyPair;
  protected final BigDecimal price;
  protected final BidAsk bidAsk;
  protected final BigDecimal newAmount;
  protected final Date timestamp;
  
  public CurrencyPairBookDepthData(String currencyPair, BigDecimal price, BidAsk bidAsk, BigDecimal newAmount, Date timestamp) {
    this.currencyPair = currencyPair;
    this.price = price;
    this.bidAsk = bidAsk;
    this.newAmount = newAmount;
    this.timestamp = timestamp;
  }
  
  public static CurrencyPairBookDepthData createFromSnapshot(String[] snapshot, String pair) {
    BigDecimal price = new BigDecimal(snapshot[0]);
    BigDecimal amount = new BigDecimal(snapshot[2]);
    
    BidAsk bidAsk = BidAsk.BID;
    if (amount.compareTo(BigDecimal.ZERO) < 0)
      bidAsk = BidAsk.ASK;
    
    return new CurrencyPairBookDepthData(pair, price, bidAsk, amount.abs(), new Date());
  }
  
  public static CurrencyPairBookDepthData createFromUpdate(String[] update, Map<String, String> chanIdPairMap) {
    String pair = chanIdPairMap.get(update[0]);
    BigDecimal price = new BigDecimal(update[1]);
    BigDecimal amount = new BigDecimal(update[3]);
    
    BidAsk bidAsk = BidAsk.BID;
    if (amount.compareTo(BigDecimal.ZERO) < 0)
      bidAsk = BidAsk.ASK;
    
    if (update[2].equals("0")) { // no orders at price
      amount = BigDecimal.ZERO;
    }
    
    return new CurrencyPairBookDepthData(pair, price, bidAsk, amount.abs(), new Date());
  }

  public String getCurrencyPair() {
    return currencyPair;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BidAsk getBidAsk() {
    return bidAsk;
  }

  public BigDecimal getNewAmount() {
    return newAmount;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return "OrderBookDepthData [currencyPair=" + currencyPair + ", price=" + price + ", bidAsk=" + bidAsk + ", newAmount=" + newAmount + ", timestamp=" + timestamp + "]";
  }
  
}
