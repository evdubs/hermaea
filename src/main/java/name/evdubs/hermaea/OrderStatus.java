package name.evdubs.hermaea;

public enum OrderStatus {
  ACTIVE {
    public String toString() {
      
      return "ACTIVE";
    }
  },
  
  EXECUTED {
    public String toString() {
      
      return "EXECUTED";
    }
  },
  
  PARTIALLY_FILLED {
    public String toString() {
      
      return "PARTIALLY FILLED";
    }
  },
  
  CANCELED {
    public String toString() {
      
      return "CANCELED";
    }
  };
  
  public static OrderStatus fromString(String s) {
    
    if (s == null)
      return null;
    else if (s.startsWith(ACTIVE.toString())) 
      return ACTIVE;
    else if (s.startsWith(EXECUTED.toString())) 
      return EXECUTED;
    else if (s.startsWith(PARTIALLY_FILLED.toString())) 
      return PARTIALLY_FILLED;
    else if (s.startsWith(CANCELED.toString())) 
      return CANCELED;
    else
      return null;
  }
}
