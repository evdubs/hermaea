package name.evdubs.hermaea;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SwapTradeData extends BitfinexWebSocketData {
  protected final String currency;
  protected final BigDecimal rate;
  protected final BigDecimal amount;
  protected final int period;
  protected final int side;
  protected final double timestamp;

  public SwapTradeData(@JsonProperty("currency") String currency, @JsonProperty("rate") BigDecimal rate, @JsonProperty("amount") BigDecimal amount, @JsonProperty("period") int period,
      @JsonProperty("side") int side, @JsonProperty("timestamp") double timestamp) {
    this.currency = currency;
    this.rate = rate;
    this.amount = amount;
    this.period = period;
    this.side = side;
    this.timestamp = timestamp;
  }

  public String getCurrency() {
    return currency;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public int getPeriod() {
    return period;
  }

  public int getSide() {
    return side;
  }

  public double getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return "SwapTradeData [currency=" + currency + ", rate=" + rate + ", amount=" + amount + ", period=" + period + ", side=" + side + ", timestamp=" + timestamp + "]";
  }
  
}
