package name.evdubs.hermaea;

import org.knowm.xchange.dto.Order.OrderType;

public enum BidAsk {
  BID, ASK;
  
  public boolean equalsOrderType(OrderType ot) {
    if (OrderType.BID.equals(ot))
      return this.equals(BID);
    else if (OrderType.ASK.equals(ot))
      return this.equals(ASK);
    
    return false;
  }
}
